$(document).ready(function() {
	$('.close-btn').click(function(){
		closefancybox(false);
	});	
	
	$('.sendUpdate-btn').click(function(){
		console.log('id:'+  $('#editId').val());
		console.log('title:'+  $('#editTitle').val());
		console.log('type:'+  $('#editType').val());
		console.log('anothers:'+  $('#editAnothers').val());
		console.log('unit:'+  $('#editUnit').val());
		console.log('page:'+  $('#editPage').val());
		console.log('vol:'+  $('#editVolumes').val());
		console.log('other:'+  $('#editOtherInfo').val());
		//$(this).attr('disabled', true);
		$("button").attr('disabled', true);
		
		$.ajax({
			url: '/api/product.servlet',
			data: {
				'action': 'update',
				'id' : $('#editId').val(),
				'title' : $('#editTitle').val(),
				'type' : $('#editType').val(),
				'anothers' : $('#editAnothers').val(),
				'unit' : $('#editUnit').val(),
				'page' : $('#editPage').val(),
				'vol': $('#editVolumes').val(),
				'other' : $('#editOtherInfo').val(),
			},
			type:"POST",
			dataType:'json',
			success: function(msg){
				//var status = JSON.stringify(msg);//JSON.parse(msg);
				console.log(msg.status);
				
				var StatusInfoText = "";
				
				if(msg.status)
					StatusInfoText = "儲存成功......該狀態列將於5秒後自動關閉";
				else
					StatusInfoTextg = "儲存失敗......該狀態列將於5秒後自動關閉";
					
				ShowStatusBar(StatusInfoText, msg.status);
				
			},error:function(xhr, ajaxOptions, thrownError){ 
				console.log(xhr.status);
				console.log(thrownError);
				
				StatusInfoTextg = "儲存失敗......該狀態列將於5秒後自動關閉";
				ShowStatusBar(StatusInfoTextg, false);
			}
		});
		
		return false;
	});
	
	$('.delete-btn').click(function(){
		$("button").attr('disabled', true);
		
		$.ajax({
			url: '/api/product.servlet',
			data: {
				'action': 'delete',
				'id' : $('#editId').val(),
			},
			type:"POST",
			dataType:'json',
			success: function(msg){
				//var status = JSON.stringify(msg);//JSON.parse(msg);
				console.log(msg.status);
				
				var StatusInfoText = "";
				
				if(msg.status)
					StatusInfoText = "刪除中，請稍候......該狀態列將於刪除完成後5秒後自動關閉";
				else
					StatusInfoText = "刪除中失敗......該狀態列將於5秒後自動關閉";
					
				ShowStatusBar(StatusInfoText, msg.status);
				
			},error:function(xhr, ajaxOptions, thrownError){ 
				console.log(xhr.status);
				console.log(thrownError);
				
				StatusInfoTextg = "刪除中失敗......該狀態列將於5秒後自動關閉";
				ShowStatusBar(StatusInfoTextg, false);
			}
		});
		
		return false;
	});
	
	$('#editResearchDisplay').bootstrapSwitch();
	
	function ShowStatusBar(msg, isStatus){
		console.log(msg);
		$(".statusBar").text(msg);
		
		if(isStatus){
			$(".statusBar").removeClass('bg-warning').addClass('bg-danger');
		}else{
			$(".statusBar").removeClass('bg-danger').addClass('bg-warning');
		
		}$(".statusBar").show();
		
		setTimeout(updateDone, 5000);
	}
	
	function sleep(sec) {
	    var time = new Date().getTime();
	    while(new Date().getTime() - time < sec * 1000);
	}
	
	function updateDone(){
		$(".statusBar").hide();
		closefancybox(true);
	}
	
	function closefancybox(isReload){
		parent.$.fancybox.close();
		
		if(isReload)
			parent.location.reload(true);
	}
});