<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">請登入</h3>
				</div>

				<s:if test="status == -1">
					<div id="alert_statusbar" class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<strong><s:property value="msg"/></strong>
					</div>
				</s:if>
				
				<div class="panel-body">
				
					<form action="" method="post">
						<input type="hidden" name="hiddenVal" value="login">
						
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="E-mail" name="email" type="email"  autocomplete="off" autofocus>
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="password" type="password" autocomplete="off" value="">
							</div>
							<div class="checkbox">
								<label>
									<input name="remember" type="checkbox" value="Remember Me">Remember Me
								</label>
							</div>
							
							<input type="submit" class="btn btn-lg btn-success btn-block" value="登入">
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>