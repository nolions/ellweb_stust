<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>	
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<ol class="breadcrumb" style="background-color:#fff; padding-left:0; padding-right:0;">
				<li><a href="/console"><i class="fa fa-home"></i> Home</a></li>
				<s:include value="../_breadcrumbItem.jsp">
					<s:param name="link">/console/Research/list</s:param>
					<s:param name="label">研究方向</s:param>
				</s:include>
				<s:include value="../_breadcrumbItem.jsp">
					<s:param name="label">新增研究方向</s:param>
				</s:include>
			</ol>
			<h2 class="page-header" style="margin-top:10px;">新增研究方向</h2>
		</div>
	</div>
	
	<div id="advisor_container" class="content_container">
		<form action="" method="post">
			<input type="hidden" name="hiddenVal" value="addresearch">
			
			<!-- modal-body -->
			<div class="modal-body">
				<div class="alert alert-danger hide">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  					<span class="sr-only">Error:</span>
					<label id="alertMsg"></label>
				</div>
			
				<div class="input-group" style="width:100%">
					<div class="input-group-addon" style="width:120px">研究名稱</div>
					<input type="text" id="Name" class="form-control" name="Name" value="" placeholder="請輸入研究方向" style="width:100%">
				</div>
				<div class="input-group" style="width:100%">
					<div class="input-group-addon" style="width:120px">英文名稱</div>
					<input type="text" id="eName" class="form-control" name="eName" value="" placeholder="請輸入研究方向英文名稱" style="width:100%">
				</div>
				<div class="input-group" style="width:100%">
					<div class="input-group-addon" style="width:120px">參考來源</div>
					<input type="text" id="Source" class="form-control" name="Source" value="" placeholder="請輸入研究方向參考文獻網址" style="width:100%">
				</div>
				<div class="input-group" style="width:100%">
					<div class="input-group-addon" id="basic-addon1" style="width:120px">參考日期</div>
					<input type="date" id="ReadTime" class="form-control" name="ReadTime" value="<s:property value="research.ReadTime" />" style="width:100%;" placeholder="2014/09/18" >
					<div class="input-group-addon">
                       	<span class="glyphicon glyphicon-calendar"></span>
                   	</div>
				</div>
				<div class="input-group" style="width:100%">
					<div class="input-group-addon" style="width:120px">描述說明</div>
					<textarea id="Descride" name="Descride" rows="10" cols="55" style="width:100%;"></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary sendUpdate-btn"><i class="fa fa-save"></i> 儲存</button>
			</div>
			<!-- /modal-footer -->
		</form>
	</div>
</div>
<link href="<%=AssetsUrl%>/css/console/form.css" rel="stylesheet">