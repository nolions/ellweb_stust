package DataAccess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;

public class GAE_DataStoreAccess {

	//The comparison operator 
	final static public FilterOperator _EQUAL = FilterOperator.EQUAL;
	final static public FilterOperator _LESS_THAN = FilterOperator.LESS_THAN;
	final static public FilterOperator _LESS_THAN_OR_EQUAL = FilterOperator.LESS_THAN_OR_EQUAL;
	final static public FilterOperator _GREATER_THAN = FilterOperator.GREATER_THAN;
	final static public FilterOperator _GREATER_THAN_OR_EQUAL = FilterOperator.GREATER_THAN_OR_EQUAL;
	final static public FilterOperator _NOT_EQUAL = FilterOperator.NOT_EQUAL;
	final static public FilterOperator _IN = FilterOperator.IN;
		
	//sort
	final static public SortDirection _Sort_Desc = SortDirection.DESCENDING;
	final static public SortDirection _Sort_ASC = SortDirection.ASCENDING;
	
	private String dbTable;
	
	private DatastoreService dsService;
	
	public GAE_DataStoreAccess(String dbTable){
		this.dbTable = dbTable;
		
		dsService = DatastoreServiceFactory.getDatastoreService();
	}
	
	/**
	 * Build a  data Entity for Google Cloud Datastore
	 * @param dbName Google Cloud Datastore Entity name
	 * @param data Data Model's property list
	 * @return
	 */
	private Entity BuildEntity(String dbName, Map<String, Object> map) {
		System.out.println("com.model.DataAccess.GAE_DataStoreAccess::BuildEntity()");
		
		Entity employe = new Entity(dbName);
		
		for ( String key : map.keySet() ) {
			employe.setProperty(key.toString(), map.get(key));
		}
		
		return employe;
	}
	
	private Entity BuildEntity(Entity employe, Map<String, Object> map) {
		System.out.println("com.model.DataAccess.GAE_DataStoreAccess::BuildEntity();");
		
		//Entity employee = new Entity(dbName);
		for ( String key : map.keySet() ) {
			employe.setProperty(key.toString(), map.get(key));
		}
		
		return employe;
	}
	
	public Entity SingleQuerySingle(Query q) {
		System.out.println("com.model.DataAccess.GAE_DataStoreAccess::SingleQuerySingle()");
		PreparedQuery pq = dsService.prepare(q);
		
		Entity entity = pq.asSingleEntity();
		
		return entity;
	}
	
	/**
	 * 
	 * @param q Prepared Query
	 * @param count limit for query
	 * @return
	 */
	public List<Entity> Query(Query q, int count) {
		System.out.println("com.model.DataAccess.GAE_DataStoreAccess::Query()");
		// Use PreparedQuery interface to retrieve results
		PreparedQuery pq = dsService.prepare(q);
		
		List<Entity> DataList = new ArrayList<Entity>();
		
		try {
			if (count == -1) 
				DataList = pq.asList(FetchOptions.Builder.withDefaults());
			else
				DataList = pq.asList(FetchOptions.Builder.withLimit(count));
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return DataList;
	}
	
	public Entity getData(String Id){
		System.out.println("com.model.DataAccess.GAE_DataStoreAccess::getData()");
		Filter filter = new FilterPredicate("Id", _EQUAL, Id);
		Query query =  new Query(dbTable).setFilter(filter);
		
		Entity data = SingleQuerySingle(query);
		
		return data;
	}
	
	public Entity getData(String key, String Id){
		System.out.println("com.model.DataAccess.GAE_DataStoreAccess::getData()");
		Filter filter = new FilterPredicate(key, _EQUAL, Id);
		Query query =  new Query(dbTable).setFilter(filter);
		
		Entity data = SingleQuerySingle(query);
		
		return data;
	}
	
	public List<Entity> getDataList(HashMap<String, Object> paramsMap, int limit) {
		System.out.println("com.model.DataAccess.GAE_DataStoreAccess::getDataList()");
		return this.getDataList(paramsMap, limit, null, null);
	}
	
	public List<Entity> getDataList(HashMap<String, Object> paramsMap, int limit, String columnName, SortDirection sort) {
		System.out.println("com.model.DataAccess.GAE_DataStoreAccess::getDataList()");
		List<Entity> data  = new ArrayList<Entity>();
		
		List<Filter> filterList = new ArrayList<Filter>();
		for(Entry<String, Object> entry : paramsMap.entrySet()) {
			Filter filter = new FilterPredicate(entry.getKey(), _EQUAL, entry.getValue());
			filterList.add(filter);
		}
		
		Query quesy = new Query(dbTable);
		if(columnName != null && sort != null)
			quesy.addSort(columnName, sort);
		
		if (filterList.size() >1){
			CompositeFilter compositeFilter = new CompositeFilter(CompositeFilterOperator.AND, filterList);
			quesy.setFilter(compositeFilter);
		}else if (filterList.size() == 1) {
			Filter filter = filterList.get(0);
			quesy.setFilter(filter);
		}
		
		
		if (limit > 0) 
			data = Query(quesy, limit);
		else
			data = Query(quesy, -1);
		
		return data;
	}
	
	/**
	 * Store single data to Google Cloud Datastore
	 * @param dbName Datastore's Entity
	 * @param data Data Model's property list
	 * @return
	 */
	public boolean Save(Map<String, Object> data) {
		System.out.println("com.model.DataAccess.GAE_DataStoreAccess::Save()");
		boolean isSuccess = false;
		
		Entity employee = BuildEntity(this.dbTable, data);
		
		try {
			dsService.put(employee);
			isSuccess = true;
			System.out.println("com.model.DataAccess.GAE_DataStoreAccess::Save(), save success");
		} catch (Exception e) {
			// TODO: handle exception
			isSuccess = false;
			System.out.println("com.model.DataAccess.GAE_DataStoreAccess::Save(), save success");
		}
		return isSuccess;
	}
	
	/**
	 * Batch store data to Google Cloud Datastore
	 * @param tableName Table name
	 * @param dataList Data's model of array list
	 * @return
	 */
	public boolean BatchSave(ArrayList<Map<String, Object>> dataList) {
		System.out.println("com.model.DataAccess.GAE_DataStoreAccess::BatchSave()");
		boolean isSuccess = false;
		
		List<Entity> employees = new ArrayList<>();
		
		for (int i = 0; i < dataList.size(); i++) {
			Map<String, Object> map = dataList.get(i);
			
			Entity employee = BuildEntity(this.dbTable, map);
			employees.add(employee);
		}
		
		try {
			dsService.put(employees);
			isSuccess = true;
			System.out.println("com.model.DataAccess.GAE_DataStoreAccess::BatchSave(), save success");
		} catch (Exception e) {
			// TODO: handle exception
			isSuccess = false;
			System.out.println("com.model.DataAccess.GAE_DataStoreAccess::BatchSave(), save fail");
		}
		
		return isSuccess;
	}
	
	public boolean Update(String id, Map<String, Object> map){
		System.out.println("com.model.DataAccess.GAE_DataStoreAccess::Update()");
		boolean isSuccess = true;
		
		try {
			Entity employe = this.getData(id);
			employe = BuildEntity(employe, map);
			dsService.put(employe);
			System.out.println("com.model.DataAccess.GAE_DataStoreAccess::Update(), update: success");
		} catch (Exception e) {
			isSuccess = false;
			System.out.println("com.model.DataAccess.GAE_DataStoreAccess::Update(), update: fail");
		}
		
		return isSuccess;
	}
	
	/**
	 * Delete data based on key of employee from Google Cloud Datastore
	 * @param employeeKey key of employee
	 * @return 
	 */
	public boolean Delete(String id) {
		System.out.println("com.model.DataAccess.GAE_DataStoreAccess::Delete()");
		boolean isSuccess = true;
		
		try {
			Entity entity = this.getData(id);
			
			dsService.delete(entity.getKey());
			System.out.println("com.model.DataAccess.GAE_DataStoreAccess::Delete(), delete : success");
		} catch (Exception e) {
			isSuccess = false;
			System.out.println("com.model.DataAccess.GAE_DataStoreAccess::Delete(), delete : fail");
		}
		
		return isSuccess;
	}
	
	public boolean Delete(String key, String id) {
		System.out.println("com.model.DataAccess.GAE_DataStoreAccess::Delete()");
		boolean isSuccess = true;
		
		try {
			Entity entity = this.getData(key, id);
			System.out.println(entity.getProperty("productsID"));
			dsService.delete(entity.getKey());
			System.out.println("com.model.DataAccess.GAE_DataStoreAccess::Delete(), delete : success");
		} catch (Exception e) {
			isSuccess = false;
			System.out.println("com.model.DataAccess.GAE_DataStoreAccess::Delete(), delete : fail");
		}
		
		return isSuccess;
	}
}
