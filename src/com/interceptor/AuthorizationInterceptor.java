package com.interceptor;

import java.util.Map;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
 
@SuppressWarnings("serial")
public class AuthorizationInterceptor extends AbstractInterceptor {
	@Override
	public String intercept(ActionInvocation actionInvocation) throws Exception {
    	Map<String, Object> session = actionInvocation.getInvocationContext().getSession();
    	
    	String id = (String) session.get("id");

    	if (id == null) {
    		return Action.LOGIN;
    	}else{
    		return actionInvocation.invoke();
    	}
    }
}
