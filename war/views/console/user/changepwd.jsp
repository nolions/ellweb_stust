<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>

<div class="container-fluid">
	<div class="row">
		<s:include value="../_pageheader.jsp">
			<s:param name="title">變更密碼</s:param>
		</s:include>
	</div>
	
	<s:if test="status != 0">
		<s:if test="status == 1">
			<s:set name="type">success</s:set>
		</s:if>
		<s:elseif test="status == -1">
			<s:set name="type">warning</s:set>
		</s:elseif>
		
		<div id="alert_statusbar" class="alert alert-<s:property value="type"/>">
			<a href="#" class="close" data-dismiss="alert">&times;</a>
			<strong><s:property value="msg"/></strong>
		</div>
	</s:if>
	
	<form action="" method="post">
			<input type="hidden" name="hiddenVal" value="changepwd">
			<input type="hidden" name="userId" value="<s:property value="userinfo.Id" />">
			
			<!-- modal-body -->
			<div class="modal-body">
				<div class="input-group">
					<span class="input-group-addon">輸入您的舊密碼</span>
					<input id="oldpassword" class="form-control" type="password" name="oldpassword" value="" placeholder="請輸入您的舊密碼" required="required" autofocus>
				</div>
				<div class="input-group">
					<span class="input-group-addon">輸入您的新密碼</span>
					<input id="newpassword" class="form-control" type="password" name="newpassword" value="" placeholder="請輸入您的新密碼" required="required">
				</div>
				<div class="input-group">
					<span class="input-group-addon">再次輸入新密碼</span>
					<input id="renewpassword" class="form-control" type="password" name="renewpassword" value="" placeholder="請再次輸入新密碼" required="required">
				</div>
			</div>
			<!-- /modal-body -->
			
			<div class="modal-footer">
				<button type="submit" id="submitBtn" class="btn btn-danger btn-lg" style="">
					<i class="fa fa-save"></i> 變更密碼
				</button>
			</div>
			<!-- /modal-footer -->
		</form> 
</div>
<link href="<%=AssetsUrl%>/css/console/user/changepwd.css" rel="stylesheet">