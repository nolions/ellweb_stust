<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>

<link href="<%=AssetsUrl%>/css/professor.css" rel="stylesheet">

<!-- Page Content -->
<div class="container" id="main-container">
	<div class="row">
		<div class="col-md-3 mainmenubar">
			<p class="lead"></p>
			
			<div class="col-md-2 list-group bs-docs-sidebar hidden-print hidden-xs hidden-sm affix">
				<ul class="nav bs-docs-sidenav">
					<li class="list-group-item ">
						<a href="#page-top" class="page-scroll">基本資料</a>
					</li>
					<!-- <li class="list-group-item">
						<a href="#educations" class="page-scroll">學歷</a>
					</li> -->
					<!-- <li class="list-group-item">
						<a href="#works" class="page-scroll">經歷</a>
					</li> -->
					<li class="list-group-item">
						<a href="#articles" class="page-scroll">著作</a>
					</li>
					<li class="list-group-item">
						<a href="#project" class="page-scroll">計畫</a>
					</li>
					<!-- <li class="list-group-item">
						<a href="#page-top" class="page-scroll">返回頂端</a>
					</li> -->
					<li class="list-group-item">
						<a href="/" class="page-scroll">回首頁</a>
					</li>
				</ul>
			</div>
		</div>
		
		<div class="col-md-9">
			<!-- Page Heading -->
			<div class="">
				<h1 class="page-header">指導教授
					<small>Advisor.</small>
				</h1>
			</div>
			<!-- end Page Heading -->
	
			<div id="photo" class="section"style="padding-top:0;">
				<div class="thumbnail">
					<img class="img-responsive" src="../assets/img/team/cjz.jpg" alt="">
				</div>
			</div>
			
			<div id="basicinfo" class="section">
				<div class="well">
					<div class="text-left">
						<button class="btn btn-success">基本資訊</button>
					</div>
					
					<hr>
					
					<div class="caption-full">
						<h3><s:property value="userinfo.Name" /> | <s:property value="userinfo.eName" /> 教授</h3>
						<p>
							<span class="glyphicon glyphicon-education bold"> 學歷：</span>
							<s:property value="userinfo.education" />
						</p>
						<p>
							<span class="glyphicon glyphicon-inbox bold"> 專長及研究領域：</span>
							<s:property value="@com.ell.model.users@toResearchsListStr(userinfo.research_areas)" />
							<%//=research_areasStr %>
						</p>
						<p>
							<span class="glyphicon glyphicon-home bold"> 研究室：</span>
							<s:property value="userinfo.lab" />
							<%//=userinfo.lab %>
						</p>
						<p>
							<span class="glyphicon glyphicon-phone-alt bold"> 分機號碼：</span>
							<s:property value="userinfo.tel" />
						</p>
						<p>
							<span class="glyphicon glyphicon-envelope bold"> E-Mail：</span>
							<a href="mailto:<s:property value="userinfo.email" />">
								<s:property value="userinfo.email" />
							</a>
						</p>
					</div>
				</div>
			</div>
			
			
			<%-- <div id="educations" class="section" style="">
				<s:include value="./_header.jsp"/>
				<jsp:include page="./_experience.jsp" flush="true">
					<jsp:param value="學歷" name="title"/>
					<jsp:param value="<%=UserId%>" name="UserId"/>
					<jsp:param value="education" name="type"/>
					<jsp:param value="2" name="col"/>
				</jsp:include>
			</div> --%>
			
			<%-- <div id="works" class="section" style="">
				<jsp:include page="./_experience.jsp" flush="true">
					<jsp:param value="相關經歷" name="title"/>
					<jsp:param value="<%=UserId%>" name="UserId"/>
					<jsp:param value="work" name="type"/>
					<jsp:param value="3" name="col"/>
				</jsp:include>
			</div> --%>
			
			<div id="articles" class="section">
				
				<s:include value="./_productsList.jsp">
					<s:param name="col">1</s:param>
					<s:param name="title">著作 - 期刊</s:param>
					<s:param name="type">journal</s:param>
					<%-- <jsp:param value="<%//=UserId%>" name="UserId"/> --%>
				</s:include>
			</div>
			
			<div id="study" class="section">
				<s:include value="./_productsList.jsp">
					<s:param name="col">2</s:param>
					<s:param name="title">著作 - 研討會</s:param>
					<s:param name="type">conference</s:param>
					<%-- <jsp:param value="<%//=UserId%>" name="UserId"/> --%>
				</s:include>
			</div>
			
			<div id="project" class="section">
				<s:include value="./_productsList.jsp">
					<s:param name="col">3</s:param>
					<s:param name="title">計畫</s:param>
					<s:param name="type">grants</s:param>
					<%-- <jsp:param value="<%//=UserId%>" name="UserId"/> --%>
				</s:include>
			</div>
			
		</div>
	</div>
</div>

<hr>
