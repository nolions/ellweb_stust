package com.ell.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.google.appengine.api.datastore.Entity;

import DataAccess.GAE_DataStoreAccess;

public class user{
	final static public int _noAction = 0;
	final static public int _success = 1;
	final static public int _fail = -1;
	
	final static public int _dataIsNull = -1;
	
	final static public int _noAccount = 2;
	final static public int _errorPW = 3;
	final static public int _reCheckPW = 4;
	
	public String Id;
	public String nickname;
	public String email;
	public String password;
	public String IPAddress;
	public Date CreateTime;
	public Date UpdateTime; 
	public Date LastLoginTime;
	public boolean EnableLogin = true;
	
	private String dbTable = "user";
	
	public user(){
		Id = UUID.randomUUID().toString();
		password = lib.Encrypt.EncryptSHA1("1234");
		CreateTime = lib.FormatDateTime.getNowTimeZoneDate(0);
		UpdateTime = CreateTime;
	}
	
	public user(String Id){
		this.Id = Id;
	}
	
	public boolean chechAccoundExistbyEmail(String AcciundEmail){
		boolean isExist = false;
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("email", AcciundEmail);
		
		GAE_DataStoreAccess mode = new GAE_DataStoreAccess(this.dbTable);
		List<Entity> entityList = mode.getDataList(paramsMap, -1);
		
		if (entityList.size() > 0)
			isExist = true;
		
		return isExist;
	}
	
	public boolean chechAccoundExistbyId(String Id){
		boolean isExist = false;
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("Id", Id);
		
		GAE_DataStoreAccess mode = new GAE_DataStoreAccess(this.dbTable);
		List<Entity> entityList = mode.getDataList(paramsMap, -1);
		
		if (entityList.size() > 0)
			isExist = true;
		
		return isExist;
	}
	
	public boolean checkAuthAccount(String email, String password){
		boolean isSuccess = false;
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("email", email);
		paramsMap.put("password", password);
		
		GAE_DataStoreAccess mode = new GAE_DataStoreAccess(this.dbTable);
		List<Entity> entityList = mode.getDataList(paramsMap, -1);
		
		if(entityList.size() > 0)
			isSuccess = true;
		
		return isSuccess;
	}
	
	public user getData(){
		GAE_DataStoreAccess mode = new GAE_DataStoreAccess(this.dbTable);
		Entity entity = mode.getData(this.Id);
		
		return EntityToModel(entity);
	}
	
	public user getDatabyMail(String email){
		GAE_DataStoreAccess mode = new GAE_DataStoreAccess(this.dbTable);
		Entity entity = mode.getData("email", email);
		
		return EntityToModel(entity);
	}
	
	public List<user> getDataList(HashMap<String, Object> paramsMap, int limit) {
		GAE_DataStoreAccess mode = new GAE_DataStoreAccess(this.dbTable);
		List<Entity> entityList = mode.getDataList(paramsMap, limit);
		
		List<user> data = new ArrayList<>();
		for (Entity entity : entityList) {
			user model = EntityToModel(entity);
			data.add(model);
		}
		
		return data;
	}
	
	public boolean StoreData(){
		Map<String, Object> dataList = ModeltoMapList(this);
		
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		return gaeDSAmodel.Save(dataList);
	}
	
	public boolean BatchStoreData(ArrayList<user> modelList){
		ArrayList<Map<String, Object>> dataList = new ArrayList<>();
		
		for (int i = 0; i < modelList.size(); i++) {
			user model = modelList.get(i);
			
			Map<String, Object> map = ModeltoMapList(model);
			
			dataList.add(map);
		}
		
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		return gaeDSAmodel.BatchSave(dataList);
	}
	
	public boolean UpdateData(){
		this.UpdateTime = lib.FormatDateTime.getNowTimeZoneDate(0);
		Map<String, Object> dataList = ModeltoMapList(this);
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		
		return gaeDSAmodel.Update(this.Id, dataList);
	}
	
	static private user EntityToModel(Entity entity){
		user model = new user();
		model.Id = entity.getProperty("Id").toString();
		model.email = entity.getProperty("email").toString();
		model.password = entity.getProperty("password").toString();
		model.CreateTime = (Date) entity.getProperty("CreateTime");
		model.UpdateTime = 	(Date) entity.getProperty("UpdateTime");
		model.EnableLogin = (boolean) entity.getProperty("EnableLogin");
		
		model.IPAddress = (entity.getProperty("IPAddress") != null)?entity.getProperty("IPAddress").toString():"";
		model.nickname = (entity.getProperty("nickname") != null)?entity.getProperty("nickname").toString():"";
		model.LastLoginTime = (entity.getProperty("LastLoginTime") != null)?(Date) entity.getProperty("LastLoginTime"):null;
		
		return model;
	}
	
	static private Map<String, Object> ModeltoMapList(user model){
		Map<String, Object> dataMap = new HashMap<>();
		dataMap.put("Id", model.Id);
		dataMap.put("email", model.email);
		dataMap.put("password", model.password);
		dataMap.put("EnableLogin", model.EnableLogin);
		
		dataMap.put("CreateTime", model.CreateTime);
		dataMap.put("UpdateTime", model.UpdateTime);
		
		if (model.IPAddress != null) dataMap.put("IPAddress", model.IPAddress);
		if (model.nickname != null) dataMap.put("nickname", model.nickname);
		if (model.LastLoginTime != null) dataMap.put("LastLoginTime", model.LastLoginTime);
		
		return dataMap;
	}
}
