<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="alert alert-danger hide">
	<a href="#" class="close" data-dismiss="alert">&times;</a>
	<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  	<span class="sr-only">Error:</span>
	<label id="alertMsg"></label>
</div>
			
<div class="input-group">
	<div class="input-group-addon">成果類型</div>
	<s:select id="type" class="dropdown selectpicker form-control" name="type"
		list="#{'journal':'期刊論文', 'conference':'研討會論文', 'grants':'研究計畫','industry':'產學合作'}"
		value="product.type">
	</s:select>       
</div>
	
<div class="input-group">
	<div class="input-group-addon">標題</div>
	<input type="text" id="title" class="form-control" name="title" value="<s:property value="product.title" />" placeholder="請輸入成果標題，ex：THE SYSTEMATIC CONSTRUCTION OF A VALID DOMAIN ONTOLOGY">
</div>
	
<div class="input-group">
	<div class="input-group-addon">作者</div>
	<input type="text" id="anothers" class="form-control" name="anothers" value="<s:property value="product.anothers" />" placeholder="成果作者，ex:Yi-Hsing Chang, Yen-Yi Chen, Nian-Shing Chen">
</div>
		
<div class="input-group">
	<div class="input-group-addon">發表單位</div>
	<input type="text" id="unit" class="form-control" name="unit" value="<s:property value="product.unit" />" placeholder="請輸入會議/刊物/單位名稱">
</div>
	
<div class="input-group">
	<div class="input-group-addon">頁數</div>
	<input type="text" id="page" class="form-control" name="page" value="<s:property value="product.page" />" placeholder="請輸入頁數，ex: pp. 1273-128">
</div>
	
<div class="input-group">
	<div class="input-group-addon">卷號</div>
	<input type="text" id="vol" class="form-control" name="vol" value="<s:property value="product.vol" />" placeholder="請輸入卷號，ex: Vol.4, No.4">
</div>

<div class="input-group">
	<div class="input-group-addon" id="basic-addon1">發表日期(From)</div>
	<input type="date" id="from" class="form-control" name="from" value="<s:date name="product.from" format="yyyy-MM-dd" />" placeholder="2014/09/18" >
	<div class="input-group-addon">
		<span class="glyphicon glyphicon-calendar"></span>
	</div>
</div>
		
<div class="input-group">
	<div class="input-group-addon" id="basic-addon1">發表日期(To)</div>
	<input type="date" id="to" class="form-control" name="to" value="<s:date name="product.to" format="yyyy-MM-dd" />" style="width:100%;" placeholder="2014/09/18" >
	<div class="input-group-addon">
		<span class="glyphicon glyphicon-calendar"></span>
	</div>
	</div>
	
<div class="input-group">
	<div class="input-group-addon">其他資訊</div>
	<input type="text" id="other" class="form-control" name="other" value="<s:property value="product.other" />" placeholder="請輸入其他關於該研究成果資訊">
</div>