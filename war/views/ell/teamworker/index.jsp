<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>
<link href="<%=AssetsUrl%>/css/userlist.css" rel="stylesheet">
<link href="<%=AssetsUrl%>/css/_userlistinfo.css" rel="stylesheet">

<!-- Page Content -->
<div class="container">
	<div class="row col-md-12">
        
    	<!-- Page Heading -->
    	<div>
    		<h1 class="page-header">研究團隊
    			<label class="sessionNumber">
    				<s:property value="classId" />
    				<%//=sessionClass %>級
    			</label>
    			<small>Team Workers.</small>
    		</h1>
    	</div>
    	<!-- end Page Heading -->
    	
    	<div>
    		<form action="/TeamWorker" method="get">
    			級別：
    			<s:select id="yearDoropList" class="dropdown selectpicker" name="year"
					list="#{103:'103', 102:'102', 101:'101', 100:'100', 99: '99'}"
					value="year">
				</s:select>
    			<input class="btn btn-danger" id="sendBtn" type="submit" value="查詢" />
			</form>
		</div>
	</div>
</div>
<!-- end Page Content -->

<!-- Page Content -->
<div class="container">
	<div class="row col-md-12">
		<hr>
		<div class="user_content">
		<s:if test="teamWorker.size() == 0">
			查無任何相關資料，請重新選擇。
		</s:if>
		
		<%int i =0; %>
		<s:iterator value="teamWorker" var="user">
			<div class="row">
			<% if(i%2==0){%>
				<!-- Header Section -->
				<s:include value="./_listinfo_left.jsp">
					<s:param name="class" value="session"/>
					<s:param name="Id" value="Id"/>
					<s:param name="name" value="Name"/>
					<s:param name="eName" value="eName"/>
					<s:param name="email" value="email"/>
					<s:param name="research" value="@lib.DataTypeParser@ConvertJSONArrayToString(research_areas, '，')"/>
					<s:param name="masterPaper" value="masterPaper" />
				</s:include>
			<% }else{ %>
				<s:include value="./_listinfo_right.jsp">
					<s:param name="class" value="session"/>
					<s:param name="Id" value="Id"/>
					<s:param name="name" value="Name"/>
					<s:param name="eName" value="eName"/>
					<s:param name="email" value="email"/>
					<s:param name="research" value="@lib.DataTypeParser@ConvertJSONArrayToString(research_areas, '，')"/>
					<s:param name="masterPaper" value="masterPaper" />
				</s:include>
			<% } %>
			</div>
			<!-- /.row -->
			<hr>
			<% i++; %>
			</s:iterator>
		</div>
	
		<!-- Pagination -->
		<!-- 
		<div class="row text-center">
			<div class="col-lg-12">
				<ul class="pagination">
					<li>
						<a href="#">&laquo;</a>
					</li>
					<li class="active">
						<a href="#">1</a>
					</li>
					<li>
						<a href="#">2</a>
					</li>
					<li>
						<a href="#">3</a>
					</li>
					<li>
						<a href="#">4</a>
					</li>
					<li>
						<a href="#">5</a>
					</li>
					<li>
						<a href="#">&raquo;</a>
					</li>
				</ul>
			</div>
		</div>
		 -->
		<!-- end Pagination -->
			
		<hr>	
	</div>
</div>

<script src="<%=AssetsUrl%>/js/ell/teamworker/index.js"></script>