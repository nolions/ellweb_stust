$(document).ready(function() {
	$('.lightboxEdit').fancybox({
		width : 600,
		height : 400,
		padding : 1,
		closeClick : false,
		closeBtn : false,
		helpers : {
			overlay : {closeClick: false} 
		}
	});
});