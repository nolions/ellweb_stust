package com.ell.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ell.models.another;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class TeamWorkerAction extends ActionSupport{
	private int year = 103;
	private List<another> teamWorker = new ArrayList<>();

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public List<another> getTeamWorker(){
		return teamWorker;
	}
	
	public void setTeamWorker(int year){
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("type", "student");
		paramsMap.put("session", year);
		
		another model = new another(paramsMap);
		teamWorker = model.getDataList(-1);
	}
	
	public String execute() {
		this.setTeamWorker(this.year);
		
		return "SUCCESS";
	}
}
