<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!-- Header -->
<header>
	<div class="container">
		<div class="intro-text">
			<div class="intro-lead-in">Welcome To Ell Lab for STUST!</div>
			<div class="intro-heading">南臺科大 數位學習研究室</div>
			<a href="#about" class="page-scroll btn btn-xl">
				更多關於我們 
				<span class="glyphicon glyphicon-chevron-right"></span>
			</a>
		</div>
	</div>
</header>