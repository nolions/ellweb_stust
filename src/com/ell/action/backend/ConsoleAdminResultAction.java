package com.ell.action.backend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ell.models.another;
import com.ell.models.mappingAnotherProducts;
import com.ell.models.mappingResearchsProducts;
import com.ell.models.products;
import com.ell.models.researchs;
import com.ell.models.user;

import DataAccess.GAE_DataStoreAccess;
import lib.FormatDateTime;

public class ConsoleAdminResultAction {
	private int status = user._noAction;
	private String msg = "";
	
	public String hiddenVal;
	public String Id;
	public String title;
	public String unit;
	public String type;
	public String anothers;
	public String vol;
	public String page;
	public String other;
	public String language;
	public String from;
	public String to;
	
	public List<String> anothersCheckBox;
	public List<String> researchsCheckBox;
	public List<String> anothersChecked;
	public List<String> researchsChecked;
	
	private List<products> productsList = new ArrayList<>();
	private List<HashMap<String, String>> anothersList = new ArrayList<>();
	private List<HashMap<String, String>> researchsList = new ArrayList<>();
	private products product;
	
	public ConsoleAdminResultAction(){
		 
	}
	
	public int getStatus(){
		return status;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public String getHiddenVal() {
		return hiddenVal;
	}

	public String getId() {
		return Id;
	}
	
	public String getTitle() {
		return title;
	}

	public String getUnit() {
		return unit;
	}
	
	public String getType() {
		return type;
	}

	public String getVol() {
		return vol;
	}
	
	public String getPage() {
		return page;
	}

	public String getOther() {
		return other;
	}

	public String getLanguage() {
		return language;
	}
	
	public String getFrom() {
		return from;
	}

	public String getTo() {
		return to;
	}
	
	public List<String> getResearchsCheckBox() {
		return researchsCheckBox;
	}
	
	public List<String> getAnothersCheckBox() {
		return anothersCheckBox;
	}
	
	public List<String> getResearchsChecked(){
		return researchsChecked;
	}
	
	public List<String> getAnothersChecked(){
		return anothersChecked;
	}
	
	/**
	 * get product information
	 * @return users
	 */
	public products getProduct(){
		return product;
	}
	
	public List<products> getProductsList() {
		return productsList;
	}
	
	/**
	 * get advisor data list
	 * @return List<users> Advisor array list data
	 */
	public List<HashMap<String, String>> getAnothersList(){
		return anothersList;
	}
	
	/**
	 * get all Research data of List
	 * @return List<researchs>
	 */
	public List<HashMap<String, String>> getResearchsList(){
		return researchsList;
	}
	
	public void setStatus(int status){
		this.status = status;
	}
	
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public void setHiddenVal(String hiddenVal) {
		this.hiddenVal = hiddenVal;
	}

	public void setId(String id) {
		Id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public void setType(String type) {
		this.type = type;
	}

	public void setVol(String vol) {
		this.vol = vol;
	}

	public void setPage(String page) {
		this.page = page;
	}

	
	public void setOther(String other) {
		this.other = other;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	public void setFrom(String from) {
		this.from = from;
	}

	public void setTo(String to) {
		this.to = to;
	}
	
	public void setResearchsCheckBox(List<String> researchsCheckBox) {
		this.researchsCheckBox = researchsCheckBox;
	}
	
	public void setAnothersCheckBox(List<String> anothersCheckBox) {
		this.anothersCheckBox = anothersCheckBox;
	}
	
	public void setProductsList(int count) {
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		
		products model = new products();
		productsList = model.getDataList(paramsMap, count, "from", GAE_DataStoreAccess._Sort_Desc);
	}
	
	/**
	 * setting data list about advisor
	 */
	public void setAnothersList(){
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		
		another model = new another(paramsMap);
		List<another> datas = model.getDataList(-1);
		
		for(another data : datas){
			String anotherData = "";
			
			if(data.type.equals(another._professor))
				anotherData = "指導教授";
			else if (data.session != -1)
				anotherData = data.session + "級";
			
			anotherData = anotherData + " "+ data.Name;
			
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("Id", data.Id);
			map.put("anotherData", anotherData);
			
			anothersList.add(map);
		}
	}
	
	/**
	 * setting all Research data based on disable
	 * @param Boolean isDisable 
	 */
	public void setResearchsList(Boolean isDisable){
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("isDisable", true);
		
		researchs model = new researchs();
		List<researchs> datas = model.getDataList(paramsMap, -1);
		
		for(researchs data : datas){
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("Id", data.Id);
			map.put("research", data.Name + " " + data.eName);
			
			researchsList.add(map);
		}
	}
	
	public void setAnothersChecked(String id){
		mappingAnotherProducts model = new mappingAnotherProducts();
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("productsID", id);
		
		anothersChecked = new ArrayList<>();
		for(mappingAnotherProducts data : model.getDataList(paramsMap)){
			anothersChecked.add(data.anotherID);
		}
	}
	
	public void setResearchsChecked(String id){
		mappingResearchsProducts model = new mappingResearchsProducts();
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("productsID", id);
		
		researchsChecked = new ArrayList<>();
		for(mappingResearchsProducts data : model.getDataList(paramsMap)){
			researchsChecked.add(data.researchID);
		}
	}
	
	/**
	 * setting product information data by id
	 * @param String UserId
	 */
	public void setProduct(String id){
		products model = new products(id);
		product = model.getData();
	}
	
	public String list(){
		this.setProductsList(-1);
		return "list";
	}
	
	public String edit(){
		this.setProduct(Id);
		this.setAnothersChecked(Id);
		this.setResearchsChecked(Id);
		
		if (hiddenVal != null && hiddenVal.equals("editresult")) {
			product.type = this.type;
			product.title = this.title;
			product.anothers = this.anothers;
			product.unit = this.unit;
			product.page = this.page;
			product.vol = this.vol;
			product.other = this.other;
			product.from = lib.FormatDateTime.ConverterStringToDate(this.from, "yyyy-MM-dd");
			product.to = lib.FormatDateTime.ConverterStringToDate(this.to, "yyyy-MM-dd");
			
			if (product.from != null)
				product.publicdate = FormatDateTime.ConverterDateToString(product.from, "yyyy/MM/dd");
			
			if (product.to != null) {
				if(product.publicdate != null && product.publicdate != "" && product.publicdate.length() > 0)
					product.publicdate = product.publicdate + "~" + FormatDateTime.ConverterDateToString(product.to, "yyyy/MM/dd");
				else
					product.publicdate = FormatDateTime.ConverterDateToString(product.to, "yyyy/MM/dd");
			}
			
			if (product.UpdateData()) {
				if (anothersCheckBox != null) {
					mappingAnotherProducts MAPmodel = new mappingAnotherProducts();
					
					MAPmodel.DeleteData("productsID", product.Id);
					
					for(String another : anothersCheckBox){
						MAPmodel = new mappingAnotherProducts();
						MAPmodel.anotherID = another;
						MAPmodel.productsID = product.Id;
						MAPmodel.StoreData();
					}
					anothersChecked = anothersCheckBox;
				}
				
				if (researchsCheckBox != null) {
					
					mappingResearchsProducts MRPmodel = new mappingResearchsProducts();
					MRPmodel.DeleteData("productsID", product.Id);
					
					for(String research : researchsCheckBox){
						MRPmodel = new mappingResearchsProducts();
						MRPmodel.researchID = research;
						MRPmodel.productsID = product.Id;
						MRPmodel.StoreData();
					}
					researchsChecked = researchsCheckBox;
				}
				
				this.setStatus(user._success);
				this.setMsg("儲存成功...");
			}else{
				this.setStatus(user._fail);
				this.setMsg("儲存失敗...");
			}
		}
		
		//get all Research list
		this.setResearchsList(true);
		this.setAnothersList();
		
		return "edit";
	}
	
	public String add(){
		if (hiddenVal != null && hiddenVal.equals("addresult")) {
			product = new products();
			product.type = this.type;
			product.title = this.title;
			product.anothers = this.anothers;
			product.unit = this.unit;
			product.page = this.page;
			product.vol = this.vol;
			product.other = this.other;
			product.from = lib.FormatDateTime.ConverterStringToDate(this.from, "yyyy-MM-dd");
			product.to = lib.FormatDateTime.ConverterStringToDate(this.to, "yyyy-MM-dd");
			
			if (product.from != null)
				product.publicdate = FormatDateTime.ConverterDateToString(product.from, "yyyy/MM/dd");
			
			if (product.to != null) {
				if(product.publicdate != null && product.publicdate != "" && product.publicdate.length() > 0)
					product.publicdate = product.publicdate + "~" + FormatDateTime.ConverterDateToString(product.to, "yyyy/MM/dd");
				else
					product.publicdate = FormatDateTime.ConverterDateToString(product.to, "yyyy/MM/dd");
			}
			
			if (product.StoreData()) {
				if (anothersCheckBox != null) {
					for(String another : anothersCheckBox){
						mappingAnotherProducts MAPmodel = new mappingAnotherProducts();
						MAPmodel.anotherID = another;
						MAPmodel.productsID = product.Id;
						MAPmodel.StoreData();
					}
				}
				
				if (researchsCheckBox != null) {
					for(String research : researchsCheckBox){
						mappingResearchsProducts MRPmodel = new mappingResearchsProducts();
						MRPmodel.researchID = research;
						MRPmodel.productsID = product.Id;
						MRPmodel.StoreData();
					}
				}
				
				return "list";
			}
		}
		
		//get all Research list
		this.setResearchsList(true);
		this.setAnothersList();
		
		return "add";
	}
	
	public String delete() {
		if (Id != null && !Id.equals("") && Id.length() > 0) {
			products model = new products(Id);
			
			if (model.DeleteData()) {
				return "list";
			}
		}
		return "delete";
	}
}
