<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="container-fluid">
	<div class="row">
		<s:include value="./_pageheader.jsp">
			<s:param name="title">權限管理</s:param>
		</s:include>
	</div>
</div>