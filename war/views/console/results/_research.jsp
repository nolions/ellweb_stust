<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="input-group">
	<div class="input-group-addon">研究方向</div>
	<div class="form-control" style="height:auto;max-height: 150px;min-height: 30px;overflow: auto;">
		<s:checkboxlist 
			id = "Modal_Research"
			list = "researchsList" 
			listKey = "Id" 
			listValue = "research" 
			name = "researchsCheckBox" 
			value = "researchsChecked"
			theme="vertical-checkbox"/>
	</div>
</div>