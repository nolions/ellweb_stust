<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>

<div class="container-fluid">
	<div class="row">
		<s:include value="../_pageheader.jsp">
			<s:param name="title">帳號設定</s:param>
		</s:include>
	</div>
	
	<!-- modal-body -->
	<div class="modal-body">
		<div class="input-group">
			<div class="input-group-addon">信箱</div>
			<div class="form-control"><s:property value="userinfo.email" /></div>
		</div>
		<div class="input-group">
			<div class="input-group-addon">暱稱</div>
			<div class="form-control"><s:property value="userinfo.nickname" /></div>
		</div>
		<div class="input-group">
			<div class="input-group-addon">最後登入IP</div>
			<div class="form-control"><s:property value="userinfo.IPAddress" /></div>
		</div>
		<div class="input-group">
			<div class="input-group-addon">帳號建立時間</div>
			<div class="form-control"><s:date name="userinfo.CreateTime" format="yyyy/MM/dd hh:mm:ss"/></div>
		</div>
		<div class="input-group">
			<div class="input-group-addon">最後更新時間</div>
			<div class="form-control"><s:date name="userinfo.UpdateTime" format="yyyy/MM/dd hh:mm:ss"/></div>
		</div>
	</div>
	<!-- /modal-body -->
			
	<div class="modal-footer">
		<a id="settingBtn" class="btn btn-danger btn-lg" href="<c:url value="/console/user/setting"/>">
			<i class="fa fa-save"></i> 帳號設定
		</a>
	</div>
	<!-- /modal-footer -->
</div>
<link href="<%=AssetsUrl%>/css/console/user/info.css" rel="stylesheet">