<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<ol class="breadcrumb" style="background-color:#fff; padding-left:0; padding-right:0;">
				<li><a href="/console"><i class="fa fa-home"></i> Home</a></li>
				<s:include value="../_breadcrumbItem.jsp">
					<s:param name="label">研究成果</s:param>
				</s:include>
			</ol>
			<h2 class="page-header" style="margin-top:10px;">研究成果</h2>
		</div>
		<%-- <s:include value="../_pageheader.jsp">
			<s:param name="title">研究成果</s:param>
			<s:param name="breadcrumbList" value="breadcrumbList" />
		</s:include> --%>
	</div>
	
	<div id="researchs_container" class="content_container">
		<div class="">
			<a class="btn btn-primary" href="<c:url value="/console/Result/add"/>" role="button">
				<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 新增
			</a>
		</div>
		<br>
		<div id="result_container" class="content_container">
			<div class="list-group">
			<s:iterator value="productsList" var="products">
				<div id="<s:property value="Id"/>_media" class="media list-group-item">
					<div class="media-body">
						<h4 class="media-heading">
							<s:property value="title"/>
						</h4>
						<label>作者：</label><s:property value="anothers"/><br>
						<label>刊物/會議/單位：</label><s:property value="unit"/>, <s:property value="vol"/>, <s:property value="page"/>, <s:property value="publicdate"/>.
					</div>
					<div class="media-right">
						<a class="btn btn-default btn-sm" href="<c:url value="/console/Result/edit?Id=${Id}"/>">編輯</a>
						<br><br>
						<a class="btn btn-danger btn-sm" href="<c:url value="/console/Result/delete?Id=${Id}"/>">刪除</a>
					</div>
				</div>
			</s:iterator>
			</div>
		</div>
	</div>
</div>