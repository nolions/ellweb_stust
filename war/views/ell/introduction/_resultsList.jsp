<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!-- Results Section -->
<section id="results">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">研究成果</h2>
				<h3 class="section-subheading text-muted">Research Results.</h3>
			</div>
		</div>
		
		<div class="row col-md-10 col-md-offset-1" >
			<table class="table table-striped table-hover table-condensed">
				<thead>
					<tr><th></th><th></th></tr>
				</thead>
				
				<tbody>
					<s:iterator value="ProductsList" var="product">
					<tr>
						<th>#</th>
						<td>
							<s:property value="@com.ell.models.products@toReferenceStr(#product)"/>
						</td>
					</tr>
					</s:iterator>
				</tbody>
			</table>
			
			<div class="text-center">
				<a class="btn btn-danger" href="<c:url value="/Products" />">
					More 
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				</a>
			</div>
		</div>
		
	</div>
</section>