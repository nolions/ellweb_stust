package com.ell.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.appengine.api.datastore.Entity;

import DataAccess.GAE_DataStoreAccess;

public class mappingAnotherProducts {
	public String anotherID;
	public String productsID;
	
	private String dbTable = "mapping_another_products";
	
	public mappingAnotherProducts(){
		
	}
	
	public List<mappingAnotherProducts> getDataList(HashMap<String, Object> paramsMap) {
		GAE_DataStoreAccess mode = new GAE_DataStoreAccess(dbTable);
		List<Entity> entityList = mode.getDataList(paramsMap, 0);
		
		List<mappingAnotherProducts> data = new ArrayList<>();
		for (Entity entity : entityList) {
			mappingAnotherProducts model = EntityToModel(entity);
			data.add(model);
		}
		
		return data;
	}
	
	public boolean StoreData(){
		Map<String, Object> dataList = ModeltoMapList(this);
		
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		return gaeDSAmodel.Save(dataList);
	}
	
	public boolean BatchStoreData(ArrayList<mappingAnotherProducts> modelList){
		ArrayList<Map<String, Object>> dataList = new ArrayList<>();
		
		for (int i = 0; i < modelList.size(); i++) {
			mappingAnotherProducts model = modelList.get(i);
			
			Map<String, Object> map = ModeltoMapList(model);
			
			dataList.add(map);
		}
		
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		return gaeDSAmodel.BatchSave(dataList);
	}
	
	public boolean DeleteData(String key, String id){
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		return gaeDSAmodel.Delete(key, id);
	}
	
	static private mappingAnotherProducts EntityToModel(Entity entity){
		mappingAnotherProducts model = new mappingAnotherProducts();
		model.productsID = entity.getProperty("productsID").toString();
		model.anotherID = entity.getProperty("anotherID").toString();
		
		return model;
	}
	
	static public Map<String, Object> ModeltoMapList(mappingAnotherProducts model){
		Map<String, Object> dataMap = new HashMap<>();
		dataMap.put("anotherID", model.anotherID);
		dataMap.put("productsID", model.productsID);
		
		return dataMap;
	}
	
}
