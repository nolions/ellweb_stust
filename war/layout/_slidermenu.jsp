<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	String AssetsUrl = request.getContextPath()+"/assets";	
%>

<!-- slider mwnu -->
<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">
			<!-- <li class="sidebar-search">
				<div class="input-group custom-search-form" style="height:20px;">
					<input type="text" class="form-control" placeholder="Search...">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</li> -->
			
			<li>
				<a href="<c:url value="/console/Advisor/list"/>"><i class="fa fa-user fa-fw"></i> 指導教授</a>
			</li>
			<li>
				<a href="<c:url value="/console/Teamworker/list"/>"><i class="fa fa-mortar-board fa-fw"></i> 研究團隊</a>
			</li>
			<%-- <li>
				<a href="#"><i class="fa fa-users fa-fw"></i> 研究團隊<span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li>
						<a href="<c:url value="/Advisor/list"/>"><i class="fa fa-user fa-fw"></i> 指導教授</a>
					</li>
					<li>
						<a href="<c:url value="/Teamworker/list"/>"><i class="fa fa-mortar-board fa-fw"></i> 研究生</a>
					</li>
					
				</ul> 
			</li>--%>
			<li>
				<a href="<c:url value="/console/Research/list"/>"><i class="fa fa-bank"></i> 研究方向</a>
			</li>
			
			<li>
				<a href="<c:url value="/console/Result/list"/>"><i class="fa fa-list-alt fa-fw"></i> 研究成果</a>
			</li>
			<li>
				<a href="#"><i class="fa fa-eye fa-fw"></i> 權限管理</a>
			</li>
			<li>
				<a href="#"><i class="fa fa-user fa-fw"></i> User管理</a>
			</li>
			<li>
				<a href="#"><i class="fa fa-info-circle fa-fw"></i> 網站資訊<span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li>
						<a href="<c:url value="/console/log"/>"><i class="fa fa-list-ol fa-fw"></i> 更新日誌</a>
					</li>
				</ul>
			</li>
			<!-- <li>
				<a href="#"><i class="fa fa-wrench fa-fw"></i> 網站設定<span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li>
						<a href="purview"><i class="fa fa-lock fa-fw"></i> 權限管理</a>
					</li>
					
					<li>
						<a href="#"><i class="fa fa-info fa-fw"></i> 網站資訊 <span class="fa arrow"></span></a>
						<ul class="nav nav-third-level">
							<li>
								<a href="#"><i class="fa fa-home fa-fw"></i> 網站資訊</a>
							</li>
							
							<li>
								<a href="#"><i class="fa fa-envelope fa-fw"></i> 聯絡資訊</a>
							</li>
							
							<li>
								<a href="#"><i class="fa fa-language fa-fw"></i> 語系</a>
							</li>
						</ul>
					</li>
				</ul>
			</li> -->
		</ul>
	</div>
	<!-- /.sidebar-collapse -->
</div>
<!-- end slider mwnu -->