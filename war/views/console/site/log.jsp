<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags" %>	
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>

<link rel="stylesheet" href="<%=AssetsUrl%>/product/DataTables/css/jquery.dataTables.css">

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<ol class="breadcrumb" style="background-color:#fff; padding-left:0; padding-right:0;">
				<li><a href="/console"><i class="fa fa-home"></i> Home</a></li>
				<s:include value="../_breadcrumbItem.jsp">
					<s:param name="label">網站日誌</s:param>
				</s:include>
			</ol>
			<h2 class="page-header" style="margin-top:10px;">網站日誌</h2>
		</div>
	</div>
	
	<div class="content_container">
		<div class="">
			<table id="logTable" class="table table-striped table-condensed hover order-column">
			<thead>
				<tr>
					<th>#</th>
					<th style="width:90px;">日期</th>
					<th>日誌</th>
				</tr>
			</thead>
			
			<tbody>
			<s:iterator value="devlogList" var="devlog">
				<tr>
					<td></td>
					<td><s:property value="date"/></td>
					<td><s:property value="log"/></td>
				</tr>
			</s:iterator>
			</tbody>
			
		</table>
		</div>
	</div>
</div>

<script src="<%=AssetsUrl%>/product/DataTables/js/jquery.dataTables.js"></script>
<script src="<%=AssetsUrl%>/js/console/site/log.js"></script>
