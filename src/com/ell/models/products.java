package com.ell.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query.SortDirection;

import DataAccess.GAE_DataStoreAccess;
import lib.FormatDateTime;

public class products{
	
	public String Id;
	public String title;
	public String unit;
	public String type;
	public String anothers;
	public String vol;
	public String page;
	public String other;
	public String language;
	public String publicdate;
	public Date from;
	public Date to;
	public Date CreateTime;
	public Date UpdateTime;
	public boolean isDisable = true;
	
	private String dbTable = "products";
	
	public products() {
		Id = UUID.randomUUID().toString();
		language = "Traditional";
		CreateTime = FormatDateTime.getNowTimeZoneDate(0);
		UpdateTime = CreateTime;
	}
	
	public products(String Id) {
		this.Id = Id;
	}
	
	public products getData(){
		GAE_DataStoreAccess mode = new GAE_DataStoreAccess(dbTable);
		Entity entity = mode.getData(this.Id);
		
		return EntityToModel(entity);
	}
	
	public products getData(String Id){
		GAE_DataStoreAccess mode = new GAE_DataStoreAccess(dbTable);
		Entity entity = mode.getData(Id);
		
		return EntityToModel(entity);
	}
	
	public List<products> getDataList(HashMap<String, Object> paramsMap, int limit){
		return this.getDataList(paramsMap, limit, null, null);
	}
	
	public List<products> getDataList(HashMap<String, Object> paramsMap, int limit, String columnName, SortDirection sort){
		GAE_DataStoreAccess mode = new GAE_DataStoreAccess(dbTable);
		List<Entity> entityList = mode.getDataList(paramsMap, limit, columnName, sort);
		
		List<products> data = new ArrayList<>();
		for (Entity entity : entityList) {
			products model = EntityToModel(entity);
			data.add(model);
		}
		
		return data;
	}
	
	public boolean StoreData(){
		Map<String, Object> dataList = ModeltoMapList(this);
		
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		return gaeDSAmodel.Save(dataList);
	}
	
	public boolean BatchStoreData(ArrayList<products> modelList){
		ArrayList<Map<String, Object>> dataList = new ArrayList<>();
		
		for (int i = 0; i < modelList.size(); i++) {
			products model = modelList.get(i);
			
			Map<String, Object> map = ModeltoMapList(model);
			
			dataList.add(map);
		}
		
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		return gaeDSAmodel.BatchSave(dataList);
	}
	
	public boolean UpdateData(){
		Map<String, Object> dataList = ModeltoMapList(this);
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		
		return gaeDSAmodel.Update(this.Id, dataList);
	}
	
	public boolean DeleteData(){
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		return gaeDSAmodel.Delete(this.Id);
	}
	
	static public String toReferenceStr(products pd){
		System.out.println("com.model.products::toReferenceStr()");
		
		String Reference = "";
		
		if (pd.anothers != null)
			Reference = Reference + pd.anothers + ", ";
		
		Reference = Reference + pd.title + ", ";
		Reference = Reference + pd.unit + ", ";
		
		if (pd.vol != null) 
			Reference = Reference + pd.vol + ", ";
		if (pd.page != null) 
			Reference = Reference + pd.page + ", ";
		if (pd.from != null)
			Reference = Reference + FormatDateTime.ConverterDateToString(pd.from, "yyyy") + ". "; 
		
		if (pd.other != null) 
			Reference = Reference + "("+pd.other+ ")";
		
		return Reference;
	}
	
	static private products EntityToModel(Entity entity){
		products model = new products();
		model.Id = entity.getProperty("Id").toString();
		model.title = (entity.getProperty("title") != null)?entity.getProperty("title").toString():null;
		model.anothers = (entity.getProperty("anothers") != null)?entity.getProperty("anothers").toString():null;
		model.unit = (entity.getProperty("unit") != null)?entity.getProperty("unit").toString():null;
		model.type = (entity.getProperty("type") != null)?entity.getProperty("type").toString():null;
		model.language = (entity.getProperty("language") != null)?entity.getProperty("language").toString():null;
		model.vol = (entity.getProperty("vol") != null)?entity.getProperty("vol").toString():null;
		model.page = (entity.getProperty("page") != null)?entity.getProperty("page").toString():null;
		model.other = (entity.getProperty("other") != null)?entity.getProperty("other").toString():null;
		model.from = (entity.getProperty("from") != null)?(Date)(entity.getProperty("from")):null;
		model.to = (entity.getProperty("to") != null)?(Date)(entity.getProperty("to")):null;
		model.publicdate = (entity.getProperty("publicdate") != null)?entity.getProperty("publicdate").toString():null;
		model.CreateTime = (Date) entity.getProperty("CreateTime");
		model.UpdateTime = (Date) entity.getProperty("UpdateTime");
		model.isDisable = (boolean) entity.getProperty("isDisable");
		
		return model;
	}
	
	/**
	 * Build products data Map List. Converter products model data to Map's ArrayList
	 * @param model products data model
	 * @return products data's Map list
	 */
	static public Map<String, Object> ModeltoMapList(products model){
		Map<String, Object> dataMap = new HashMap<>();
		dataMap.put("Id", model.Id);
		dataMap.put("title", model.title);
		dataMap.put("type", model.type);
		dataMap.put("language", model.language);
		dataMap.put("isDisable", model.isDisable);
		
		dataMap.put("CreateTime", model.CreateTime);
		dataMap.put("UpdateTime", model.UpdateTime);
		
		if (model.anothers != null) dataMap.put("anothers", model.anothers);
		if (model.unit != null) dataMap.put("unit", model.unit);
		if (model.vol != null) dataMap.put("vol", model.vol);
		if (model.page != null) dataMap.put("page", model.page);
		if (model.other != null) dataMap.put("other", model.other);
		if (model.from != null) dataMap.put("from", model.from);
		if (model.to != null) dataMap.put("to", model.to);
		if (model.publicdate != null) dataMap.put("publicdate", model.publicdate);
			
		return dataMap;
	}
	
	static public products MapListtoModel(Map<String, Object> map){
		products model = new products();
		
		if (map.get("title") != null) 
			model.title = map.get("title").toString();
		
		if (map.get("type") != null)
			model.type = map.get("type").toString();
		
		if (map.get("unit") != null)
			model.unit = map.get("unit").toString();
		
		if (map.get("anothers") != null)
			model.anothers = map.get("anothers").toString();
		
		if (map.get("language") != null)
			model.language = map.get("language").toString();
		
		if (map.get("page") != null)
			model.page = map.get("page").toString();
		
		if (map.get("vol") != null)
			model.vol = map.get("vol").toString();
		
		if (map.get("other") != null)
			model.other = map.get("other").toString();
		
		if (map.get("from") != null)
			model.from = FormatDateTime.ConverterStringToDate(map.get("from").toString(), "yyyy/MM/dd");
		
		if (map.get("to") != null)
			model.to = FormatDateTime.ConverterStringToDate(map.get("to").toString(), "yyyy/MM/dd");
		
		if (model.from != null)
			model.publicdate = FormatDateTime.ConverterDateToString(model.from, "yyyy/MM/dd");
		
		if (model.to != null) {
			if(model.publicdate != null && model.publicdate != "" && model.publicdate.length() > 0)
				model.publicdate = model.publicdate + "~" + FormatDateTime.ConverterDateToString(model.to, "yyyy/MM/dd");
			else
				model.publicdate = FormatDateTime.ConverterDateToString(model.to, "yyyy/MM/dd");
		}
		
		return model;
	}
	
	static public products MapListtoModel(Map<String, Object> map, products model){
		if (map.get("title") != null) 
			model.title = map.get("title").toString();
		
		if (map.get("type") != null)
			model.type = map.get("type").toString();
		
		if (map.get("unit") != null)
			model.unit = map.get("unit").toString();
		
		if (map.get("anothers") != null)
			model.anothers = map.get("anothers").toString();
		
		if (map.get("language") != null)
			model.language = map.get("language").toString();
		
		if (map.get("page") != null)
			model.page = map.get("page").toString();
		
		if (map.get("vol") != null)
			model.vol = map.get("vol").toString();
		
		if (map.get("other") != null)
			model.other = map.get("other").toString();
		
		if (map.get("from") != null)
			model.from = FormatDateTime.ConverterStringToDate(map.get("from").toString(), "yyyy/MM/dd");
		
		if (map.get("to") != null)
			model.to = FormatDateTime.ConverterStringToDate(map.get("to").toString(), "yyyy/MM/dd");
		
		if (model.from != null)
			model.publicdate = FormatDateTime.ConverterDateToString(model.from, "yyyy/MM/dd");
		
		if (model.to != null) {
			if(model.publicdate != null && model.publicdate != "" && model.publicdate.length() > 0)
				model.publicdate = model.publicdate + "~" + FormatDateTime.ConverterDateToString(model.to, "yyyy/MM/dd");
			else
				model.publicdate = FormatDateTime.ConverterDateToString(model.to, "yyyy/MM/dd");
		}
		
		model.UpdateTime = FormatDateTime.getNowTimeZoneDate(0);
		
		return model;
	}
}
