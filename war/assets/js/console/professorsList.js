$(document).ready(function() {
	$('#addbtn').click(function(){
		var userName = $('#userName').val();
		if(userName.length == 0){
			$('#alertmsg').text("姓名不能為空");
			$('#alertModal').modal('show');
		}else{
			$('#Modal_job').val($('#duties').val());
			$('#Modal_userName').val($('#userName').val());
			$('#modal-title_userName').text($('#userName').val());
			$('#formModal').modal('show');
		}
		
		return false;
	});
	
	$('.close-btn').click(function(){
		modalClose(false);
	});
	
	$('.sendAdd-btn').click(function(){
		//$('.sendAdd-btn').attr('disabled', true);
		$("button").attr('disabled', true);
		
		var checkedValue = $('input[name="research"]:checked').map(function(){
			return $(this).val();
		}).get().join(',');
		
		console.log('userid:'+  $('#editUserId').val());
		console.log('job:'+  $('#Modal_job').val());
		console.log('username:'+  $('#Modal_userName').val());
		console.log('userename:'+  $('#Modal_EName').val());
		console.log('education:'+  $('#Modal_Education').val());
		console.log('email:'+  $('#Modal_Email').val());
		console.log('tel:'+  $('#Modal_Tel').val());
		console.log('lab:'+  $('#Modal_Lab').val());
		console.log('officetime:'+  $('#Modal_OfficeTime').val())
		console.log('research:'+ checkedValue);
		
		$.ajax({
			url: '/api/user.servlet',
			data: {
				'action': 'addAdvisor',
				'job' : $('#Modal_job').val(),
				'username' : $('#Modal_userName').val(),
				'userename' : $('#Modal_EName').val(),
				'education' : $('#Modal_Education').val(),
				'email' : $('#Modal_Email').val(),
				'tel' : $('#Modal_Tel').val(),
				'lab' : $('#Modal_Lab').val(),
				'officetime' : $('#Modal_OfficeTime').val(),
				'research' : checkedValue,
			},
			type:"POST",
			dataType:'json',
			success: function(msg){
				
				//var status = JSON.stringify(msg);//JSON.parse(msg);
				console.log(msg.status);
				
				var StatusInfoText = "";
				
				if(msg.status== true)
					StatusInfoText = "儲存成功......該狀態列將於5秒後自動關閉";
				else
					StatusInfoTextg = "儲存失敗......該狀態列將於5秒後自動關閉";
					
				ShowStatusBar(StatusInfoText, msg.status);
				
			},error:function(xhr, ajaxOptions, thrownError){ 
				$(this).attr('disabled', false);
				console.log(xhr.status);
				console.log(thrownError);
				
				StatusInfoTextg = "儲存失敗......該狀態列將於5秒後自動關閉";
				ShowStatusBar(StatusInfoTextg, false);
			}
		});
		
		return false;
	});
	
	function ShowStatusBar(msg, isStatus){
		console.log(msg);
		$(".statusBar").text(msg);
		
		if(isStatus){
			$(".statusBar").removeClass('bg-warning').addClass('bg-danger');
		}else{
			$(".statusBar").removeClass('bg-danger').addClass('bg-warning');
		
		}
		
		$(".statusBar").show();
		
		setTimeout(updateDone, 5000);
	}
	
	function sleep(sec) {
	    var time = new Date().getTime();
	    while(new Date().getTime() - time < sec * 1000);
	}
	
	function updateDone(){
		$(".statusBar").hide();
		modalClose(true);
	}
	
	function modalClose(isReload){
		$('.modal').modal('hide');
		$('.sendAdd-btn').attr('disabled', false);
		
		if(isReload)
			location.reload();
	}
});