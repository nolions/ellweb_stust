$(document).ready(function() {
	$('.close-btn').click(function(){
		modalClose(false);
	});	
	
	$('#addbtn').click(function(){
		var userName = $('#userName').val();
		var sessionNo = $('#sessionNo').val();
		
		if(userName.length == 0 || sessionNo.length == 0 ){
			$('#alertmsg').text("姓名與級別不能為空");
			$('#alertModal').modal('show');
		}else{
			$('#Modal_sessionNo').val($('#sessionNo').val());
			$('#Modal_userName').val($('#userName').val());
			$('#modal-title_sessionNo').text($('#sessionNo').val());
			$('#modal-title_userName').text($('#userName').val());
			$('#formModal').modal('show');
		}
		
		return false;
	});
	
	$('.sendAdd-btn').click(function(){
		//$('.sendAdd-btn').attr('disabled', true);
		$("button").attr('disabled', true);
		
		var checkedValue = $('input[name="research"]:checked').map(function(){
			return $(this).val();
		}).get().join(',');
		console.log(checkedValue);
		
		$.ajax({
			url: '/api/user.servlet',
			data: {
				'action': 'addStudent',
				'session' : $('#Modal_sessionNo').val(),
				'username' : $('#Modal_userName').val(),
				'userename' : $('#Modal_EName').val(),
				'email' : $('#Modal_Email').val(),
				'research' : checkedValue,
				'masterPaper' : $('#Modal_PaperTitle').val(),
			},
			type:"POST",
			dataType:'json',
			success: function(msg){
				
				//var status = JSON.stringify(msg);//JSON.parse(msg);
				console.log(msg.status);
				
				var StatusInfoText = "";
				
				if(msg.status== true)
					StatusInfoText = "儲存成功......該狀態列將於5秒後自動關閉";
				else
					StatusInfoTextg = "儲存失敗......該狀態列將於5秒後自動關閉";
					
				ShowStatusBar(StatusInfoText, msg.status);
				
			},error:function(xhr, ajaxOptions, thrownError){ 
				$(this).attr('disabled', false);
				console.log(xhr.status);
				console.log(thrownError);
				
				StatusInfoTextg = "儲存失敗......該狀態列將於5秒後自動關閉";
				ShowStatusBar(StatusInfoTextg, false);
			}
		});
		
		return false;
	});
	
	function ShowStatusBar(msg, isStatus){
		console.log(msg);
		$(".statusBar").text(msg);
		
		if(isStatus){
			$(".statusBar").removeClass('bg-warning').addClass('bg-danger');
		}else{
			$(".statusBar").removeClass('bg-danger').addClass('bg-warning');
		
		}
		
		$(".statusBar").show();
		
		setTimeout(updateDone, 5000);
	}
	
	function sleep(sec) {
	    var time = new Date().getTime();
	    while(new Date().getTime() - time < sec * 1000);
	}
	
	function updateDone(){
		$(".statusBar").hide();
		modalClose(true);
	}
	
	function modalClose(isReload){
		$('.modal').modal('hide');
		$('.sendAdd-btn').attr('disabled', false);
		
		if(isReload)
			location.reload();
	}
	
//var table = $('#productlist-table').DataTable({
////	"columnDefs":[
////	       {"targets": [3], "visible": false},
////	       {"targets": [4], "visible": false},
////	       {"targets": [5], "visible": false},
////	       {"targets": [6], "visible": false},
////	],
//	//"pagingType": "full_numbers",
//	scrollCollapse:true,
//	"order": [[ 0,"desc"]],
//	paging: true,
//	"language":{
//		"lengthMenu": "每頁顯示 _MENU_ 筆",
//		"zeroRecords": "無法搜尋到相關資料",
//		"info": "第 _PAGE_ 頁 ，共 _PAGES_ 頁",
//		"infoEmpty": "",
//		"infoFiltered": "(filtered from _MAX_ total records)",
//		"paginate": {
//			"first":"第一頁",
//			"last":"最末頁",
//			"next":"下一頁",
//			"previous":"前一頁"
//		}
//	}
//});

});