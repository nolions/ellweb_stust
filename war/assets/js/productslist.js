$(document).ready(function() {
var table = $('#productlist-table').DataTable({
	"columnDefs":[
	       {"targets": [3], "visible": false},
	       {"targets": [4], "visible": false},
	       {"targets": [5], "visible": false},
	       {"targets": [6], "visible": false},
	],
	//"pagingType": "full_numbers",
	scrollCollapse:true,
//	"order": [[ 1,"desc"]],
	paging: true,
	"language":{
		"lengthMenu": "每頁顯示 _MENU_ 筆",
		"zeroRecords": "無法搜尋到相關資料",
		"info": "第 _PAGE_ 頁 ，共 _PAGES_ 頁",
		"infoEmpty": "",
		"infoFiltered": "(filtered from _MAX_ total records)",
		"paginate": {
			"first":"第一頁",
			"last":"最末頁",
			"next":"下一頁",
			"previous":"前一頁"
		}
	}
});

$('#productlist-table tbody').on('click', 'tr', function () {
    var data = table.row( this ).data();
    var title = data[1];
    var unti = data[2]
    var another = data[3];
    var date = data[4];
    var page = data[5];
    var type = data[6];
    
    var TypeNam = "";
    if(type == "journal")
    	TypeName = "刊物名稱：";
    else 
    	TypeName = "會議名稱：";
    //alert( 'You clicked on '+data[4]+'\'s row' );
    var message = title + "<br><br>"+ TypeName + unti + "<br><br>作者："+ another + "<br>日期：" + date  + "<br>頁數："+ page;
    swal({
    	title:"詳細資訊!",
    	text:message,
    	html: true,
    	type: "success"
    });
} );

table.on( 'order.dt search.dt', function () {
	table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();
});