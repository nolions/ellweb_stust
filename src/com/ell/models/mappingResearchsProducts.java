package com.ell.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.appengine.api.datastore.Entity;

import DataAccess.GAE_DataStoreAccess;

public class mappingResearchsProducts {
	public String researchID;
	public String productsID;
	
	private String dbTable = "mapping_researchs_products";
	
	public mappingResearchsProducts(){
		
	}
	
	public List<mappingResearchsProducts> getDataList(HashMap<String, Object> paramsMap) {
		GAE_DataStoreAccess mode = new GAE_DataStoreAccess(dbTable);
		List<Entity> entityList = mode.getDataList(paramsMap, 0);
		
		List<mappingResearchsProducts> data = new ArrayList<>();
		for (Entity entity : entityList) {
			mappingResearchsProducts model = EntityToModel(entity);
			data.add(model);
		}
		
		return data;
	}
	
	public boolean StoreData(){
		Map<String, Object> dataList = ModeltoMapList(this);
		
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		return gaeDSAmodel.Save(dataList);
	}
	
	public boolean DeleteData(String key, String id){
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		return gaeDSAmodel.Delete(key, id);
	}
	
	static private mappingResearchsProducts EntityToModel(Entity entity){
		mappingResearchsProducts model = new mappingResearchsProducts();
		model.productsID = entity.getProperty("productsID").toString();
		model.researchID = entity.getProperty("researchID").toString();
		
		return model;
	}
	
	static public Map<String, Object> ModeltoMapList(mappingResearchsProducts model){
		Map<String, Object> dataMap = new HashMap<>();
		dataMap.put("researchID", model.researchID);
		dataMap.put("productsID", model.productsID);

		return dataMap;
	}
}
