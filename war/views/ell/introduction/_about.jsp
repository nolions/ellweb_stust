<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!-- About Section -->
<section id="about">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">關於我們</h2>
				<h3 class="section-subheading text-muted">About Me.</h3>
			</div>
		</div>
		
		<div class="row col-lg-8 col-lg-offset-2 text-center">
			<p class="text-faded text-left">
				南臺科大數位學習研究室(E-Learning Lab of STUST)，簡稱數位學習研究室(Ell Lab)，為南臺科技大學資訊管理學系暨研究所下的研究團隊。由南臺科大資管系張儀興教授所帶領並進行指導。
			</p>
			<br>
			<p class="text-faded text-left">
				研究團隊中從事研究方向豐富且多元，如數位學習相關(情境式、行動與適性化學習)、情境式應用(RFID、Kinect與擴充實境)相關和網際網路相關(雲端服務、資料檢索、Web 2.0、Web Service與Mashup)...等領域，成員可以根據自身之興趣與專長選擇亦或是喜好來選擇研究方向/領域。過程中強調理論與實務並重，目前研究室之主要研究方向為數位學習相關應用。
			</p>
			<br>
			<p class="text-faded text-left">
				下面將會一一的詳細介紹研究室目前進行相關研究方向/領域與其相關之領域研究成果。
			</p>
			<br>
			<a href="#researchs" class="page-scroll btn btn-xl">
				查看研究方向
				<span class="glyphicon glyphicon-chevron-right"></span>
			</a>
		</div>
	</div>
</section>