<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>	
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>

<!-- DataTable CSS -->
<link rel="stylesheet" href="<%=AssetsUrl%>/product/DataTables/css/jquery.dataTables.css">
<link rel="stylesheet" href="<%=AssetsUrl%>/product/sweetalert/css/sweetalert.css">
<link href="<%=AssetsUrl%>/css/productslist.css" rel="stylesheet">

<!-- Page Content -->
<!-- Page Heading -->
<div class="container">	
	<div class="row col-lg-12">
		<h1 class="page-header">研究成果
			<small>Research Results.</small>
		</h1>
	</div>
</div>
<!-- end Page Heading -->
<!-- end Page Content -->

<div class="container">	
	<div class="row">
		<div class="col-lg-12">
			<form action="/Products" method="get">
				<div id="productslist-filter" class="row bg-warning">
					<div class="right col-lg-8">
						<div>
							研究方向：
							<s:select id="researchDoropList" class="dropdown selectpicker" name="research"
								headerKey="all" headerValue="所有方向" 
								list="ResearchsList" listKey="Name" listValue="Name"
								value="research">
							</s:select>
						</div>
						<br>
						<div>
							成果類型：
							<s:select id="typeDoropList" class="dropdown selectpicker" name="type"
								headerKey="all" headerValue="所有類型" 
								list="#{'journal':'期刊論文', 'conference':'研討會論文', 'grants':'研究計畫','industry':'產學合作'}"
								value="type">
							</s:select>
						</div>
					</div>
						
					<div id="btn-block" class="right col-lg-4">
						<input class="btn btn-lg btn-block btn-danger" id="sendBtn" type="submit" value="查詢" />
					</div>
				</div>
		
				<br>
			</form>
		</div>
	</div>
</div>

<div class="container">
	
	<div class="row col-lg-12">
	
		<table class="table table-striped table-condensed hover order-column" id="productlist-table" >
			<thead>
				<tr>
					<th></th>
					<th>標題</th>
					<th>刊物/會議/單位 名稱</th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			
			<tbody>
			<s:iterator value="ProductsList" var="Product">
				<tr>
					<th></th>
					<td><s:property value="title"/></td>
					<td><s:property value="unit"/></td>
					<td><s:property value="anothers"/></td>
					<td><s:property value="publicdate"/></td>
					<td><s:property value="page"/></td>
					<td><s:property value="type"/></td>
				</tr>
			</s:iterator>
			</tbody>
			
		</table>
		
	</div>
	
</div>

<script src="<%=AssetsUrl%>/product/DataTables/js/jquery.dataTables.js"></script>
<script src="<%=AssetsUrl%>/product/sweetalert/js/sweetalert.min.js"></script>
<script src="<%=AssetsUrl%>/js/ell/productslist/index.js"></script>
<script src="<%=AssetsUrl%>/js/productslist.js"></script>
