<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>
<!--  -->

<s:set name="col">
   ${param.col}
</s:set>
<s:set name="productType">
   ${param.type}
</s:set>

<s:if test="#col % 2 == 0">
	<s:set name="classStyle">well</s:set>
	<s:set name="hrStyle">hr2</s:set>
</s:if>
<s:else>
	<s:set name="classStyle">thumbnail</s:set>
	<s:set name="hrStyle"></s:set>
</s:else>

<s:property value="setProductlist(#productType)"/>

<!-- end -->

<!-- product list UI -->
<div class="<s:property value="classStyle"/>">
	<div class="text-left">
		<a class="btn btn-success">${param.title}</a>
	</div>
		
	<hr class="<s:property value="hrStyle"/>">
	
	<s:iterator value="Productlist">
		<div class="row">
			<div class="col-md-12">
			
			<!-- setting product unit -->
			<s:if test="unit != null">
				<s:set name="unit">
   					<s:property value="to" />
				</s:set>
				<s:property value="unit"/>
			</s:if>
			<!-- end setting product unit -->
			
				<span class="pull-right">
				<!-- setting product start time -->
				<s:if test="from != null">
					<s:date name="from" format="yyyy/MM/dd"/>
				</s:if>
				<!-- end setting product start time -->
				
				<!-- setting product end time -->
				<s:if test="to != null">
					<s:date name="to" format="yyyy/MM/dd"/>
				</s:if>
				<!-- end setting product end time -->
					
				</span>
				<p>
				<!-- setting product title -->
				<s:if test="title != null">
					<s:set name="title">
   						<s:property value="title" />
					</s:set>
					<s:property value="title"/>
				</s:if>
				<!-- end setting product title -->
				</p>
			</div>
		</div>
		
		<hr class="<s:property value="hrStyle"/>">	
	</s:iterator>
</div>
<!-- end product list UI -->