<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>	
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<ol class="breadcrumb" style="background-color:#fff; padding-left:0; padding-right:0;">
				<li><a href="/console"><i class="fa fa-home"></i> Home</a></li>
				<s:include value="../_breadcrumbItem.jsp">
					<s:param name="link">/console/Teamworker/list</s:param>
					<s:param name="label">研究團隊</s:param>
				</s:include>
				<s:include value="../_breadcrumbItem.jsp">
					<s:param name="label">新增成員</s:param>
				</s:include>
			</ol>
			<h2 class="page-header" style="margin-top:10px;">新增成員</h2>
		</div>
	</div>
	
	<div id="teamworker_container" class="content_container">
		<form action="" method="post">
			<input type="hidden" name="hiddenVal" value="addteamworker">
			
			<!-- modal-body -->
			<div class="modal-body">
				<div class="alert alert-danger hide">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  					<span class="sr-only">Error:</span>
					<label id="alertMsg"></label>
				</div>
			
				<div class="input-group"> 
					<div class="input-group-addon">級別</div>
					<input type="number" id="session" class="form-control" name="session" value="103" placeholder="請輸入您的姓名">
				</div>
				<div class="input-group">
					<div class="input-group-addon">姓名</div>
					<input type="text" id="Name" class="form-control" name="Name" value="" placeholder="請輸入您的姓名">
				</div>
				<div class="input-group">
					<div class="input-group-addon">英文名字</div>
					<input type="text" id="eName" class="form-control" name="eName" value="" placeholder="請輸入您的英文名字">
				</div>
				<div class="input-group">
					<div class="input-group-addon">E-mail</div>
					<input type="text" id="email" class="form-control" name="email" value="" placeholder="請輸入您的E-mail">
				</div>
				<div class="input-group">
					<div class="input-group-addon">論文題目</div>
					<input type="text" id="masterPaper" class="form-control" name="masterPaper" value=""  placeholder="請輸入您的論文題目">
				</div>
				<div class="input-group">
					<div class="input-group-addon">研究方向</div>
					<div class="form-control" style="height:auto;">
						<s:checkboxlist 
							id = "Modal_Research"
							list = "researchsList" 
							listKey = "Name" 
							listValue = "Name" 
							name = "research" 
							value = "researchStrs"
						/>
					</div>
				</div>
			</div>
			<!-- /modal-body -->
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary sendUpdate-btn"><i class="fa fa-save"></i> 儲存</button>
			</div>
			<!-- /modal-footer -->
		</form>
	</div>
</div>
<link href="<%=AssetsUrl%>/css/console/form.css" rel="stylesheet">