package com.ell.action.backend;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;
import com.opensymphony.xwork2.ActionSupport;

import com.ell.models.user;
import lib.FormatDateTime;

public class LoginAction extends ActionSupport implements SessionAware {
	private static final long serialVersionUID = 1L;

	public String hiddenVal;
	private String email;
	private String password;
	private int status = user._noAction;
	private String msg;
	private Map<String, Object> session = new HashMap<>();

	public String getHiddenVal() {
		return hiddenVal;
	}
	
	public Map<String, Object> getSession() {
		return session;
	}
		
	public String getEmail() {
		return email;
	}
		
	public String getPassword() {
		return password;
	}
		
	public int getStatus(){
		return status;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public void setHiddenVal(String hiddenVal) {
		this.hiddenVal = hiddenVal;
	}
	
	public void setSession(Map<String, Object> map) {
		this.session = map;
	}
        
	public void setEmail(String mail) {
		if(!mail.equals("") && mail.length() > 0)
			this.email = mail;
		else
			this.email = null;
	}
		
	public void setPassword(String psword) {
		if(!psword.equals("") && psword.length() > 0)
			this.password = lib.Encrypt.EncryptSHA1((psword));
		else
			this.password = null;
	}
		
	public void setStatus(int status){
		this.status = status;
	}
	
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public String execute(){
		return SUCCESS;
	}
	
	/**
	 * Logout from system
	 * @return
	 */
	public String logOut() {
		//session.remove("userid");
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession();
		
		session.invalidate();
		System.out.println(session);
		addActionMessage("You have been Successfully Logged Out");
		return SUCCESS;
	}

	/**
	 * Login to system by user's email and password
	 * @return 
	 */
	public String logIn() {
		System.out.println("logIn");
		String ActionStr = LOGIN;
		
		if (hiddenVal != null && hiddenVal.equals("login")) {
			if (email != null && password != null) {
				user model = new user();
				
				if (model.chechAccoundExistbyEmail(email)){
					if(model.checkAuthAccount(email, password)){
						model = model.getDatabyMail(email);
						model.LastLoginTime = FormatDateTime.getNowTimeZoneDate(0);
						model.IPAddress = lib.newwork.getIPAddress();
						
						if(model.UpdateData()){
							setSession(model);
							ActionStr = SUCCESS;
						}else{
							this.setMsg("發生錯誤請洽網站管理員。");
							this.setStatus(user._fail);
						}
					}else{
						addActionError("password is error");
						System.out.println("password is error");
						this.setMsg("帳號&密碼錯誤");
						this.setStatus(user._fail);
					}
				}else {
					addActionError("account is't exist");
					System.out.println("account is't exist");
					this.setMsg("帳號&密碼錯誤");
					this.setStatus(user._fail);
				}
			}else{
				addActionError("email or password can't be blanked");
				this.setMsg("帳號&密碼為必填寫之項目，不能為空白");
				this.setStatus(user._fail);
			}
		}
		
		return ActionStr;
     }
	
	private void setSession(user model){
		session.put("id", model.Id);
		session.put("userid", model.email);
		session.put("ipaddress", model.IPAddress);
		session.put("nickname", model.nickname);
		session.put("LastLoginTime", model.LastLoginTime);
	}
}
