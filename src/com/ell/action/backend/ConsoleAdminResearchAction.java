package com.ell.action.backend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ell.models.researchs;
import com.ell.models.user;


public class ConsoleAdminResearchAction {
	
	private int status = user._noAction;
	private String msg = "";
	
	public String hiddenVal;
	public String Id;
	public String Name;
	public String eName;
	public String Descride;
	public String Source;
	public String ReadTime;
	
	private List<researchs> researchsList = new ArrayList<>();
	private researchs research;
	
	public ConsoleAdminResearchAction(){
		 
	}
	
	public int getStatus(){
		return status;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public String getHiddenVal() {
		return hiddenVal;
	}
	
	public String getId() {
		return Id;
	}
	
	public String getName() {
		return Name;
	}

	public String geteName() {
		return eName;
	}
	
	public void setHiddenVal(String hiddenVal) {
		this.hiddenVal = hiddenVal;
	}

	public String getDescride() {
		return Descride;
	}
	
	public String getSource() {
		return Source;
	}
	
	public String getReadTime() {
		return ReadTime;
	}
	
	public List<researchs> getResearchsList(){
		return researchsList;
	}
	
	/**
	 * get research information
	 * @return users
	 */
	public researchs getResearch(){
		return research;
	}

	public void setStatus(int status){
		this.status = status;
	}
	
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public void setId(String id) {
		Id = id;
	}

	public void setName(String name) {
		Name = name;
	}

	public void seteName(String eName) {
		this.eName = eName;
	}

	public void setDescride(String descride) {
		Descride = descride;
	}

	public void setSource(String source) {
		Source = source;
	}

	public void setReadTime(String readTime) {
		ReadTime = readTime;
	}
	
	public void setResearchsList(Boolean isDisable){
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("isDisable", true);
		
		researchs model = new researchs();
		researchsList = model.getDataList(paramsMap, -1);
	}

	/**
	 * setting research information data by id
	 * @param String research Id
	 */
	public void setResearch(String Id){
		researchs model = new researchs(Id);
		research = model.getData();
	}
	
	public String list(){
		this.setResearchsList(true);
		
		return "list";
	}
	
	public String edit(){
		//get edit user id
		this.setResearch(Id);
		
		if (hiddenVal != null && hiddenVal.equals("editresearch")) {
			research.Name = this.Name;
			research.eName = this.eName;
			research.Source = this.Source;
			research.ReadTime = lib.FormatDateTime.ConverterStringToDate(this.ReadTime, "yyyy-MM-dd");
			research.Descride = this.Descride;
			System.out.println(Source);
			System.out.println(this.Source);
			
			if (research.UpdateData()) {
				this.setStatus(user._success);
				this.setMsg("儲存成功...");
			}else{
				this.setStatus(user._fail);
				this.setMsg("儲存失敗...");
			}
		}
		
		return "edit";
	}
	
	public String add(){
		if (hiddenVal != null && hiddenVal.equals("addresearch")) {
			research = new researchs();
			research.Name = this.Name;
			research.eName = this.eName;
			research.Source = this.Source;
			research.ReadTime = lib.FormatDateTime.ConverterStringToDate(this.ReadTime, "yyyy-MM-dd");
			research.Descride = this.Descride;
			
			if (research.StoreData()) {
				return "list";
			}
		}
		
		//get all Research list
		this.setResearchsList(true);
		
		return "add";
	}
	
	public String delete() {
		if (Id != null && !Id.equals("") && Id.length() > 0) {
			researchs model = new researchs(Id);
			
			if (model.DeleteData()) {
				return "list";
			}
		}
		return "delete";
	}
}
