package lib;


public class Chart {
	
	final static String googleChartAPI = "https://chart.googleapis.com/chart";
	final static String imageSize = "300x300";
	final static char errorCorrectionL = 'L';
	final static char errorCorrectionM = 'M';
	final static char errorCorrectionQ = 'Q';
	final static char errorCorrectionH = 'H';
	
	public Chart(){
		
	}
	
	static public String gteQRCodeAPI(String dataStr){
		return gteQRCodeAPI(dataStr, imageSize, errorCorrectionQ);
	}
	
	static public String gteQRCodeAPI(String dataStr, String sizeStr){
		return gteQRCodeAPI(dataStr, sizeStr, errorCorrectionQ);
	}
	
	static public String gteQRCodeAPI(String dataStr, char errorCorrection){
		return gteQRCodeAPI(dataStr, imageSize, errorCorrectionQ);
	}
	
	static public String gteQRCodeAPI(String dataStr, String sizeStr, char errorCorrection){
		String API = googleChartAPI + "?cht=qr&choe=UTF-8&";
		API =  API + "chs=" + sizeStr + "&";
		API = API + "chld=" + errorCorrection + "&" ;
		API = API + "chl=" + dataStr;
		
		return API;
	}
}
