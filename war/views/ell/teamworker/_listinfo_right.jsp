<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>

<div class="col-md-8 team-member-data">
	<h3>
		<span class="sessionNumber">${param.class}<%//=sectionClass%>級</span>
		${param.name} | ${param.eName}
	</h3>
	
	<h4>專&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;長：</h4>
	
	<h4>研究方向：${param.research}</h4>
	<h4>論文題目：${param.masterPaper}</h4>
	<p></p>
	
	<!-- <a class="btn btn-primary" href="#">
		More <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	</a> -->
</div>
						
<div class="col-md-4">
	<a href="#">
		<img class="img-responsive" src="<%=AssetsUrl%>/img/user_icon.png" alt="">
	</a>
	
	<div class="team-member">
		<ul class="list-inline social-buttons">
			<li><a href="#"><i class="fa fa-facebook"></i></a></li>
			<li><a href=""><i class="fa fa-google-plus"></i></a></li>
			<li><a href="#"><i class="fa fa-bold"></i></a></li>
		</ul>
	</div>
</div>