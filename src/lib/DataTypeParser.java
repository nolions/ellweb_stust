package lib;

import java.util.ArrayList;
import java.util.Map;

import com.google.appengine.labs.repackaged.org.json.*;

public class DataTypeParser {
	
	static public int Checkformat(Object intervention){
		int formatIdCode = -1;
		
		if (intervention instanceof JSONArray)
			formatIdCode = 0; // is JSONObject
		else if (intervention instanceof JSONObject)
			formatIdCode = 1; // is JSONObject
		else
			formatIdCode = -1; // not JSONObject and JSONArray data
		
		return formatIdCode;
	}
	
	static public boolean isJSONValid(String jsonStr){
		boolean isTrue = false;
		
		try {
	        new JSONObject(jsonStr);
	        isTrue = true;
	    } catch (JSONException ex) {
	        // edited, to include @Arthur's comment
	        // e.g. in case JSONArray is valid as well...
	        try {
	            new JSONArray(jsonStr);
	            
	            isTrue = true;
	        } catch (JSONException ex1) {
	        	isTrue = false;
	        }
	    }
	    
		return isTrue;
	}
	
	static public ArrayList<Object> ConvertStringToArrayList(String str, String mark){
		ArrayList<Object> list = new ArrayList<>();
		
		String[] items = str.replaceAll("\\[", "").replaceAll("\\]", "").split(mark);
		
		for (int i = 0; i < items.length; i++) {
			list.add(items[i]);
		}
		
		return list;
	}
	
	static public String ConvertArrayListToJSONString(ArrayList<Object> arrayList)
	{
		JSONArray json = new JSONArray(arrayList);
		return json.toString();
	}
	
	static public String ConvertJSONArrayToString(String JsonStr, String mark){
		String arrayListStr = "";
		
		if(isJSONValid(JsonStr)){
			ArrayList<String> arrayList = DataTypeParser.ConvertJSONArrayStrToArrayList(JsonStr);
		
			for(int j=0; j<arrayList.size(); j++){
				String areaStr = arrayList.get(j);
				if(arrayListStr.length()>0)
					arrayListStr = arrayListStr + mark + areaStr;
				else
					arrayListStr = areaStr;
			}
		}
		
		return arrayListStr;
	}
	
	static public ArrayList<String> ConvertJSONArrayStrToArrayList(String Str)
	{
		ArrayList<String> list= new ArrayList<>();
		
		try {
			JSONArray jsonArray = new JSONArray(Str);
			
			for (int i=0; i<jsonArray.length(); i++)
			    list.add( jsonArray.getString(i) );
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("lab.DataTypeParser::ConvertJSONArrayStrToArrayList(");
		} 
		
		return list;
	}
	
	static public ArrayList<String> ConvertJSONstringToArrayList(String jsonString)
	{
		return null;
	}

	static public JSONObject ConvertHashMaptoJSONstring(Map<String, Object> map){
		JSONObject obj = new JSONObject(map);
		return obj;
	}
	
	//TODO
	static public JSONObject EncodeJSONObject(String key, Object object) {
		JSONObject obj = new JSONObject();
		
		try {
			obj.put(key, object);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return obj;
	}
	
	static public JSONArray EncodeJSONArray(ArrayList<Object> objList) {
		JSONArray array = new JSONArray();
		
		for (int i = 0; i < objList.size(); i++)
			array.put(objList.get(i));
		
		return array;
	}
	
	static public String ConvertStringCount(String Str, int count){
		return (Str.length() < count)?Str:Str.substring(0, count);
	}
}
