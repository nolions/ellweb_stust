package com.ell.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.ell.models.products;
import com.ell.models.researchs;
import com.ell.models.another;
import com.opensymphony.xwork2.ActionSupport;

import DataAccess.GAE_DataStoreAccess;
import lib.DataTypeParser;
import lib.FormatDateTime;

@SuppressWarnings("serial")
public class IntroductionInfoAction extends ActionSupport {
	
	private List<another> AdvisorList = new ArrayList<>();
	private List<another> StudentsList = new ArrayList<>();
	private List<researchs> ResearchsList = new ArrayList<>();
	private List<products> ProductsList = new ArrayList<>();

	public List<another> getAdvisorList(){
		return AdvisorList;
	}
	
	public List<another> getStudentsList() {
		return StudentsList;
	}
	
	public List<researchs> getResearchsList() {
		return ResearchsList;
	}
	
	public List<products> getProductsList() {
		return ProductsList;
	}
	
	public void setAdvisorList(){
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("type", another._professor);
		
		another model = new another(paramsMap);
		AdvisorList = model.getDataList(-1);
	}
	
	public void setStudentsLis(int count) {
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("type", another._student);
		
		another model = new another(paramsMap);
		StudentsList = model.getDataList(count, "session", GAE_DataStoreAccess._Sort_Desc);
	}
	
	
	public void setResearchsList(Boolean isDisable) {
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("isDisable", true);
		
		researchs model = new researchs();
		ResearchsList = model.getDataList(paramsMap, -1);
	}
	
	public void setProductsList(int count) {
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		
		products model = new products();
		ProductsList = model.getDataList(paramsMap, count, "from", GAE_DataStoreAccess._Sort_Desc);
	}
	
	public String researchToString(String researchJsonStr){
		ArrayList<String> researchList = DataTypeParser.ConvertJSONArrayStrToArrayList(researchJsonStr);
		
		String researchListStr = "";
		for(int j=0; j<researchList.size(); j++){
			String areaStr = researchList.get(j);
			if(researchListStr.length()>0)
				researchListStr = researchListStr +"、"+ areaStr;
			else
				researchListStr = areaStr;
		}
		
		return researchListStr;
	}
	
	public String ConverterDateToString(Date date, String format){
		return FormatDateTime.ConverterDateToString(date, "yyyy/MM/dd");
	}
	
	public String execute() {
		this.setResearchsList(true);
		this.setAdvisorList();
		this.setStudentsLis(3);
		this.setProductsList(5);
		
		return "SUCCESS";
	}
}
