<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>	
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<ol class="breadcrumb" style="background-color:#fff; padding-left:0; padding-right:0;">
				<li><a href="/console"><i class="fa fa-home"></i> Home</a></li>
				<s:include value="../_breadcrumbItem.jsp">
					<s:param name="link">/console/Advisor/list</s:param>
					<s:param name="label">指導教授</s:param>
				</s:include>
				<s:include value="../_breadcrumbItem.jsp">
					<s:param name="label">編輯指導教授 - <s:property value="userinfo.Name" /></s:param>
				</s:include>
			</ol>
			<h2 class="page-header" style="margin-top:10px;">編輯指導教授 - <s:property value="userinfo.Name" /></h2>
		</div>
	</div>
	
	<s:if test="status != 0">
		<s:if test="status == 1">
			<s:set name="alertType">success</s:set>
		</s:if>
		<s:elseif test="status == -1">
			<s:set name="alertType">warning</s:set>
		</s:elseif>
		
		<div id="alert_statusbar" class="alert alert-<s:property value="alertType"/>">
			<a href="#" class="close" data-dismiss="alert">&times;</a>
			<strong><s:property value="msg"/></strong>
		</div>
	</s:if>
	
	<div id="teamworker_container" class="content_container">
		<form action="" method="post">
			<input type="hidden" name="hiddenVal" value="editadvisor">
			
			<!-- modal-body -->
			<div class="modal-body">
				<div class="alert alert-danger hide">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  					<span class="sr-only">Error:</span>
					<label id="alertMsg"></label>
				</div>
			
				<div class="input-group"> 
					<div class="input-group-addon">職稱</div>
					<input type="text" id="job" class="form-control" name="job" value="<s:property value="userinfo.job" />" placeholder="請輸入您的職稱">
				</div>
				<div class="input-group">
					<div class="input-group-addon">姓名</div>
					<input type="text" id="Name" class="form-control" name="Name" value="<s:property value="userinfo.Name" />" placeholder="請輸入您的姓名">
				</div>
				<div class="input-group">
					<div class="input-group-addon">英文名字</div>
					<input type="text" id="eName" class="form-control" name="eName" value="<s:property value="userinfo.eName" />" placeholder="請輸入您的英文名字">
				</div>
				<div class="input-group">
					<div class="input-group-addon">學歷</div>
					<input type="text" id="education" class="form-control" name="education" value="<s:property value="userinfo.education" />" placeholder="請輸入您的最高學歷">
				</div>
				<div class="input-group">
					<div class="input-group-addon">E-mail</div>
					<input type="text" id="email" class="form-control" name="email" value="<s:property value="userinfo.email" />" placeholder="請輸入您的E-mail">
				</div>
				<div class="input-group">
					<div class="input-group-addon">電話</div>
					<input type="text" id="tel" class="form-control" name="tel" value="<s:property value="userinfo.tel" />" placeholder="請輸入您的聯絡電話">
				</div>
				<div class="input-group">
					<div class="input-group-addon">研究室</div>
					<input type="text" id="lab" class="form-control" name="lab" value="<s:property value="userinfo.lab" />" placeholder="請輸入您的研究室">
				</div>
				<div class="input-group">
					<div class="input-group-addon">Office Time</div>
					<input type="text" id="officetime" class="form-control" name="officetime" value="<s:property value="userinfo.officetime" />" placeholder="請輸入您的Office Time">
				</div>
				<div class="input-group">
					<div class="input-group-addon">研究方向</div>
					<div class="form-control" style="height:auto;">
						<s:checkboxlist 
							id = "Modal_Research"
							list = "researchsList" 
							listKey = "Name" 
							listValue = "Name" 
							name = "research" 
							value = "researchStrs"
						/>
					</div>
				</div>
			</div>
			<!-- /modal-body -->
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary sendUpdate-btn"><i class="fa fa-save"></i> 儲存</button>
			</div>
			<!-- /modal-footer -->
		</form>
	</div>
</div>
<link href="<%=AssetsUrl%>/css/console/form.css" rel="stylesheet">