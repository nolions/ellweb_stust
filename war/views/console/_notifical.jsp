<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	String AssetsUrl = request.getContextPath()+"/assets";	
%>

<!-- info notifical bar -->
<ul class="nav navbar-top-links navbar-right">
	
	<!-- user setting notifical -->
	<li class="dropdown">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
		</a>
			
		<ul class="dropdown-menu dropdown-user">
			<li><a href="<c:url value="/console/user/info"/>"><i class="fa fa-gear fa-fw"></i> 帳號資訊</a></li>
			<li><a href="<c:url value="/console/user/setting"/>"><i class="fa fa-gear fa-fw"></i> 帳號設定</a></li>
			<%-- <li><a href="<c:url value="/user/loginLog"/>"><i class="fa fa-list fa-fw"></i> 活動紀錄</a></li> --%>
			<%-- <li><a href="<c:url value="/user/changepwd"/>"><i class="fa fa-pencil fa-fw"></i> 變更密碼</a></li> --%>
			<li class="divider"></li>
			<li><a href="/console/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
		</ul>
	</li>
	<!-- end user setting notifical -->
</ul>
<!-- end info notifical bar -->
	