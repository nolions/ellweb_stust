<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>
<link href="<%=AssetsUrl%>/css/index.css" rel="stylesheet">

<!-- Header Section -->
<s:include value="./_header.jsp"/>

<!-- About Section -->
<s:include value="./_about.jsp"/>

<!-- Research Section -->
<s:include value="./_researchsList.jsp"/>

<!-- Professor Section -->
<s:include value="./_professorList.jsp"/>

<!-- Team Member Section -->
<s:include value="./_teamMemberList.jsp"/>

<!-- Results Section -->
<s:include value="./_resultsList.jsp"/>
