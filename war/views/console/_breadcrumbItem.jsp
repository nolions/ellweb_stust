<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:set name="link">
   ${param.link}
</s:set>
<s:set name="label">
   ${param.label}
</s:set>
<li>
<s:if test="%{#link != ''}">
	<a href="<s:property value="link" />"><s:property value="label" /></a>
</s:if>
<s:else>
	<s:property value="label" />
</s:else>
</li>