package com.ell.action.backend;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.ell.models.another;
import com.ell.models.cache;
import com.ell.models.products;
import com.ell.models.researchs;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import lib.DataTypeParser;

public class APIsAction  extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ActionContext context;
	private HttpServletRequest request;
	private HashMap<String, Object> resultMap = new HashMap<String, Object>();
	
	public String result;
	
	public APIsAction(){
		context = ActionContext.getContext();
		request = (HttpServletRequest) context.get(ServletActionContext.HTTP_REQUEST);
		
		resultMap.put("status", "fail");
		resultMap.put("msg", "data error");
	}
	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		
		this.result = result;
	}

	public String apis(){
		System.out.println("aaa");
		// context = ActionContext.getContext();
    	//HttpServletRequest request = (HttpServletRequest) context.get(ServletActionContext.HTTP_REQUEST);
    	
		String rest = request.getParameter("rest");
    	String model = request.getParameter("model");
    	if (rest != null) {
		
	    	switch (rest) {
				case "get":
					this.getData(model);
					break;
				case "post":
					this.postData(model);
					break;
				case "put":
					this.putData(model);
					break;
				case "delete":
					this.deleteData(model);
					break;
		
				default:
					break;
			}
    	}
    	
    	this.setResult(DataTypeParser.ConvertHashMaptoJSONstring(resultMap).toString());
		return SUCCESS;
	}
	
	private void getData(String modelType){
		String action = request.getParameter("action");
		
		if (modelType.equals("another")) {
			if (action != null) {
				HashMap<String, Object> map = lib.newwork.parserRequestParam(request);
				map.remove("model");
				map.remove("action");
				map.remove("rest");
				
				if (action.equals("all")) {
					if (modelType.equals("another")) {
						
						cache model = new cache(modelType, map);
						System.out.println(model.getAnotherData().data);
						for (String str : lib.DataTypeParser.ConvertJSONArrayStrToArrayList(model.getAnotherData().data)) {
							try {
								JSONObject jsonObj = new JSONObject(str);
								System.out.println(jsonObj.get("name"));
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						
						resultMap.put("data", model.getAnotherData().data);
					}else if (modelType.equals("product")) {
						
					}else if (modelType.equals("researchs")) {
						
					}
				}else if(action.equals("search")){
				
				}
			}else{
				String Id = request.getParameter("Id");
				System.out.println("search another Id: " + Id);
				
				another model = new another(Id);
				model = model.getData();
				
				if (model != null) {
					resultMap.put("data", another.ModeltoMapList(model));
					resultMap.put("status", "success");
					resultMap.remove("msg");
				}
			}
		}else if (modelType.equals("product")) {
			if (action != null) {
				
				if (action.equals("all")) {
					
				}else if(action.equals("search")){
					
				}
			}else{
				String Id = request.getParameter("Id");
				System.out.println("search product Id: " + Id);
				
				products model = new products(Id);
				model = model.getData();
				
				if (model != null) {
					resultMap.put("data", products.ModeltoMapList(model));
					resultMap.put("status", "success");
					resultMap.remove("msg");
				}
			}
		}else if (modelType.equals("researchs")) {
			if (action != null) {
				
				if (action.equals("all")) {
					
				}else if(action.equals("search")){
					
				}
			}else{
				String Id = request.getParameter("Id");
				System.out.println("search research Id: " + Id);
				researchs model = new researchs(Id);
				model = model.getData();
				
				if (model != null) {
					resultMap.put("data", researchs.ModeltoMapList(model));
					resultMap.put("status", "success");
					resultMap.remove("msg");
				}
			}
		}
	}
	
	private void postData(String modelType){
		boolean isPost = false;
		HashMap<String, Object> map = lib.newwork.parserRequestParam(request);
		resultMap.remove("Id");
		
		if (modelType.equals("another")) {
			another model = another.MapListtoModel(map);
			isPost = model.StoreData();
		}else if (modelType.equals("product")) {
			products model = products.MapListtoModel(map);
			isPost = model.StoreData();
		}else if (modelType.equals("researchs")) {
			researchs model = researchs.MapListtoModel(map);
			isPost = model.StoreData();
		}
		
		if (isPost) {
			resultMap.put("status", "success");
			resultMap.remove("msg");
		}
	}
	
	private void putData(String modelType){
		boolean isPut = false;
		HashMap<String, Object> map = lib.newwork.parserRequestParam(request);
		String Id =map.get("Id").toString();
		
		if (Id != null && Id.length() > 0) {
			if (modelType.equals("another")) {
				another model = new another(Id);
				
				if (model != null){
					model = another.MapListtoModel(map, model);
					isPut = model.UpdateData();
				}
			}else if (modelType.equals("product")) {
				products model = new products(Id);
				model = model.getData();
				
				if (model != null){
					model = products.MapListtoModel(map, model);
					isPut = model.UpdateData();
				}
			}else if (modelType.equals("researchs")) {
				researchs model = new researchs(Id);
				model = model.getData();
				
				if (model != null) {
					model = researchs.MapListtoModel(map, model);
					isPut = model.UpdateData();
				}
			}
		}
		if (isPut) {
			resultMap.put("status", "success");
			resultMap.remove("msg");
		}
	}
	
	private void deleteData(String modelType){
		String Id = request.getParameter("Id");
		boolean isdel = false;
		if (modelType.equals("another")) {
			another model = new another(Id);
			isdel = model.DeleteData();
		}else if (modelType.equals("product")) {
			products model = new products(Id);
			isdel = model.DeleteData();
		}else if (modelType.equals("researchs")) {
			researchs model = new researchs(Id);
			isdel = model.DeleteData();
		}
		
		if (isdel) {
			resultMap.put("status", "success");
			resultMap.remove("msg");
		}
	}
}
