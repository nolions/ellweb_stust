package com.ell.action.backend;

import java.util.ArrayList;
//import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.ell.models.another;
import com.ell.models.researchs;
import com.ell.models.user;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class ConsoleAdminAdvisorAction extends ActionSupport{
	
	public String hiddenVal;
	public String Id;
	public String Name = "";
	public String eName = "";
	public String email = "";
	public String job = "";
	public String education = "";
	public String lab = "";
	public String tel = "";
	public String officetime = "";
	public String research_areas = "";
	public boolean isDisable = true;
	
	private List<another> advisorList = new ArrayList<>();
	private List<researchs> researchsList = new ArrayList<>();
	
	private List<String> researchStrs;
	private another userinfo;
	
	private int status = user._noAction;
	private String msg = "";
	
	public ConsoleAdminAdvisorAction(){
		
	}
	
	public String getHiddenVal() {
		return hiddenVal;
	}
	
	public String getId() {
		return Id;
	}
	
	public String getName() {
		return Name;
	}
	
	public String geteName() {
		return eName;
	}
	
	public String getEmail() {
		return email;
	}

	public String getJob() {
		return job;
	}
	
	public String getLab() {
		return lab;
	}
	
	public String getTel() {
		return tel;
	}
	
	public String getOfficetime() {
		return officetime;
	}
	
	public boolean getisDisable() {
		return isDisable;
	}
	
	/**
	 * get advisor data list
	 * @return List<users> Advisor array list data
	 */
	public List<another> getAdvisorList(){
		return advisorList;
	}
	
	/**
	 * get all Research data of List
	 * @return List<researchs>
	 */
	public List<researchs> getResearchsList(){
		return researchsList;
	}
	
	/**
	 * set user's Research list
	 * @return List<String>
	 */
	public List<String> getResearchStrs(){
		return researchStrs;
	}
	
	/**
	 * get user information
	 * @return users
	 */
	public another getUserinfo(){
		return userinfo;
	}
	
	public int getStatus(){
		return status;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public void setHiddenVal(String hiddenVal) {
		this.hiddenVal = hiddenVal;
	}

	public void setId(String id) {
		Id = id;
	}
	
	public void setName(String name) {
		Name = name;
	}
	
	public void seteName(String eName) {
		this.eName = eName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public void setLab(String lab) {
		this.lab = lab;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public void setOfficetime(String officetime) {
		this.officetime = officetime;
	}

	public void setisDisable(boolean isDisable) {
		this.isDisable = isDisable;
	}

	/**
	 * setting data list about advisor
	 */
	public void setAdvisorList(){
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("type", another._professor);
		
		another model = new another(paramsMap);
		advisorList = model.getDataList(-1);
	}
	
	/**
	 * setting all Research data based on disable
	 * @param Boolean isDisable 
	 */
	public void setResearchsList(Boolean isDisable){
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("isDisable", true);
		
		researchs model = new researchs();
		researchsList = model.getDataList(paramsMap, -1);
	}
	
	/**
	 * setting User's Research list
	 * @param String ResearchJsonStr
	 */
	public void setResearchStrs(String ResearchJsonStr){
		if(ResearchJsonStr != null && !ResearchJsonStr.equals(""))
			researchStrs = lib.DataTypeParser.ConvertJSONArrayStrToArrayList(ResearchJsonStr);
	}
	
	/**
	 * setting user information data by user id
	 * @param String UserId
	 */
	public void setUserinfo(String UserId){
		another model = new another(UserId);
		userinfo = model.getData();
		
		//set checked research list
		this.setResearchStrs(userinfo.research_areas);
	}
	
	public void setStatus(int status){
		this.status = status;
	}
	
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * Professors data list of Action
	 * @return String 
	 */
	public String list(){
		this.setAdvisorList();
		
		//get all Research list
		//this.setResearchsList(true);
		
		return "list";
	}
	
	public String add(){
		if (hiddenVal != null && hiddenVal.equals("addadvisor")) {
			userinfo = new another();
			userinfo.type = another._professor;
			
			userinfo.Name = Name;
			userinfo.eName = eName;
			userinfo.email = email;
			userinfo.education = education;
			userinfo.tel = tel;
			userinfo.officetime = officetime;
			userinfo.job = job;
			userinfo.lab = lab;
			
			if (userinfo.StoreData()) {
				return "list";
			}
		}
		
		//get all Research list
		this.setResearchsList(true);
		
		return "add";
	}
	
	/**
	 * Edit Advisor's information of Action
	 * @return String 
	 */
	public String edit(){
		this.setUserinfo(Id);
		
		if (hiddenVal != null && hiddenVal.equals("editadvisor")) {
			userinfo.Name = Name;
			userinfo.eName = eName;
			userinfo.email = email;
			userinfo.education = education;
			userinfo.tel = tel;
			userinfo.officetime = officetime;
			userinfo.job = job;
			userinfo.lab = lab;
			
			if (userinfo.UpdateData()) {
				this.setStatus(user._success);
				this.setMsg("儲存成功...");
			}else{
				this.setStatus(user._fail);
				this.setMsg("儲存失敗...");
			}
		}
		
		//get all Research list
		this.setResearchsList(true);
		
		return "edit";
	}
	
	public String delete() {
		if (Id != null && !Id.equals("") && Id.length() > 0) {
			another model = new another(Id);
			
			if (model.DeleteData()) {
				return "list";
			}
		}
		return "delete";
	}
}
