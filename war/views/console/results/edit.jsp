<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>	
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<ol class="breadcrumb" style="background-color:#fff; padding-left:0; padding-right:0;">
				<li><a href="/console"><i class="fa fa-home"></i> Home</a></li>
				<s:include value="../_breadcrumbItem.jsp">
					<s:param name="link">/console/Result/list</s:param>
					<s:param name="label">研究成果</s:param>
				</s:include>
				<s:include value="../_breadcrumbItem.jsp">
					<s:param name="label">編輯研究成果</s:param>
				</s:include>
			</ol>
			<h2 class="page-header" style="margin-top:10px;">編輯研究成果</h2>
		</div>
	</div>
	
	<s:if test="status != 0">
		<s:if test="status == 1">
			<s:set name="alertType">success</s:set>
		</s:if>
		<s:elseif test="status == -1">
			<s:set name="alertType">warning</s:set>
		</s:elseif>
		
		<div id="alert_statusbar" class="alert alert-<s:property value="alertType"/>">
			<a href="#" class="close" data-dismiss="alert">&times;</a>
			<strong><s:property value="msg"/></strong>
		</div>
	</s:if>
	
	<div id="advisor_container" class="content_container">
		<form action="" method="post">
			<input type="hidden" name="hiddenVal" value="editresult">
			<!-- modal-body -->
			<div id="baseinfo" class="modal-body">
				<s:include value="./_baseinfo.jsp"></s:include>
				<s:include value="./_research.jsp"></s:include>
				<s:include value="./_another.jsp"></s:include>
			</div>
			<!-- END modal-body -->
			
			<!-- modal-footer -->
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary sendUpdate-btn">
					<i class="fa fa-save"></i> 儲存
				</button>
			</div>
			<!-- END modal-footer -->
		</form>
	</div>
</div>

<link href="<%=AssetsUrl%>/css/console/form.css" rel="stylesheet">
<%-- <!-- Bootstrap Select CSS -->
<link rel="stylesheet" href="<%=AssetsUrl%>/product/bootstrap-select/css/bootstrap-select.css">
<!-- Bootstrap Select JavaScript -->
<script src="<%=AssetsUrl%>/product/bootstrap-select/js/bootstrap-select.js"></script> --%>