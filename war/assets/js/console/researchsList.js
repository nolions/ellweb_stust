$(document).ready(function() {
	$('#addbtn').click(function(){
		var researchName = $('#researchName').val();
		var researchsDescribe = $('#researchsDescribe').val();
		
		if(researchName.length == 0){
			$('#alertmsg').text("研究方向不能為空");
			$('#alertModal').modal('show');
		}else{
			$('#Modal_researchName').val($('#researchName').val());
			$('#Modal_researchsDescribe').val($('#researchsDescribe').val());
			$('#formModal').modal('show');
		}
		
		return false;
	});
	
	$('.close-btn').click(function(){
		modalClose(false);
	});
	
	$('.sendAdd-btn').click(function(){
		$('.sendAdd-btn').attr('disabled', true);
		
		$.ajax({
			url: '/api/researchs.servlet',
			data: {
				'action': 'create',
				'Name' : $('#Modal_researchName').val(),
				'eName' : $('#Modal_researchEName').val(),
				'Source' : $('#Modal_researchSource').val(),
				'ReadTime' : $('#Modal_researchReadDate').val(),
				'Descride' : $('#Modal_researchDescribe').val(),
				//'isDisable' : $('#Modal_researchDisplay').bootstrapSwitch('state'),
			},
			type:"POST",
			dataType:'json',
			success: function(msg){
				
				//var status = JSON.stringify(msg);//JSON.parse(msg);
				console.log(msg.status);
				
				var StatusInfoText = "";
				
				if(msg.status== true)
					StatusInfoText = "儲存成功......該狀態列將於5秒後自動關閉";
				else
					StatusInfoTextg = "儲存失敗......該狀態列將於5秒後自動關閉";
					
				ShowStatusBar(StatusInfoText, msg.status);
				
			},error:function(xhr, ajaxOptions, thrownError){ 
				$(this).attr('disabled', false);
				console.log(xhr.status);
				console.log(thrownError);
				
				StatusInfoTextg = "儲存失敗......該狀態列將於5秒後自動關閉";
				ShowStatusBar(StatusInfoTextg, false);
			}
		});
		
		return false;
	});
	
	$("#Modal_researchReadDate").datetimepicker({
		format: 'YYYY/MM/DD'
	});
	
	$('#Modal_researchDisplay').bootstrapSwitch();
	
	function ShowStatusBar(msg, isStatus){
		console.log(msg);
		$(".statusBar").text(msg);
		
		if(isStatus){
			$(".statusBar").removeClass('bg-warning').addClass('bg-danger');
		}else{
			$(".statusBar").removeClass('bg-danger').addClass('bg-warning');
		
		}
		
		$(".statusBar").show();
		
		setTimeout(updateDone, 5000);
	}
	
	function sleep(sec) {
	    var time = new Date().getTime();
	    while(new Date().getTime() - time < sec * 1000);
	}
	
	function updateDone(){
		$(".statusBar").hide();
		modalClose(true);
	}
	
	function modalClose(isReload){
		$('.modal').modal('hide');
		$('.sendAdd-btn').attr('disabled', false);
		
		if(isReload)
			location.reload();
	}
});