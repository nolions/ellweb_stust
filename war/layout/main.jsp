<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>

<!DOCTYPE html>
<html lang="zn-tw">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<link rel="Shortcut Icon" type="image/x-icon" href="<%=RootUrl%>/icon.png" />

<title><tiles:insertAttribute name="title" ignore="true" /></title>

<!-- Bootstrap Core CSS -->
<link href="<%=AssetsUrl%>/product/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Bootstrap Select CSS -->
<link rel="stylesheet" href="<%=AssetsUrl%>/product/bootstrap-select/css/bootstrap-select.css">

<!-- Custom CSS -->
<link href="<%=AssetsUrl%>/theme/agency/css/agency.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<%=AssetsUrl%>/product/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

<link href="<%=AssetsUrl%>/css/style.css" rel="stylesheet">
<link href="<%=AssetsUrl%>/css/_footer.css" rel="stylesheet">

<!-- jQuery -->
<script src="<%=AssetsUrl%>/product/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<%=AssetsUrl%>/product/bootstrap/js/bootstrap.min.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<%=AssetsUrl%>/product/bootstrap-select/js/bootstrap-select.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-78258027-1', 'auto');
  ga('send', 'pageview');

</script>

</head>

<body id="page-top" class="index">

<tiles:insertAttribute name="topbar" />
<tiles:insertAttribute name="body" />
<tiles:insertAttribute name="footer" />
	
<!-- Plugin JavaScript -->
<script src="<%=AssetsUrl%>/product/jquery.easing.min.js"></script>
<script src="<%=AssetsUrl%>/product/classie.js"></script>
<script src="<%=AssetsUrl%>/product/cbpAnimatedHeader.js"></script>

<!-- Contact Form JavaScript -->
<script src="<%=AssetsUrl%>/product/jqBootstrapValidation.js"></script>
<script src="<%=AssetsUrl%>/product/contact_me.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<%=AssetsUrl%>/theme/agency/js/agency.js"></script>

</body>
</html>