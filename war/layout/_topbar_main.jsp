<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String AssetsUrl = request.getContextPath()+"/assets";	
%>
<link href="<%=AssetsUrl%>/css/index.css" rel="stylesheet">
<nav class="main_menu navbar navbar-default navbar-fixed-top">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header page-scroll">
		
			<!-- mobile main menu button -->
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		
			<a class="navbar-brand page-scroll" href="/">
				<div id="topbar_logo">
					<img  class="img" src="<%=AssetsUrl %>/img/Ell_logo_m.png">
					Ell Lab for STUST
				</div>
			</a>
		</div>
	
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse mainmenubar" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<!-- <li class="hidden">
					<a href="#page-top"></a>
				</li> -->
				<!-- <li>
					<a class="page-scroll" href="#about">關於我們</a>
				</li> -->
				<!-- <li>
					<a class="page-scroll" href="#researchs">研究方向</a>
				</li> -->
				<!-- <li>
					<a class="page-scroll" href="#professor">指導教授</a>
				</li> -->
				<!-- <li>
					<a class="page-scroll" href="#team">研究團隊</a>
				</li> -->
				<!-- <li>
					<a class="page-scroll" href="#results">研究成果</a>
				</li> -->
				<!-- <li>
					<a class="page-scroll" href="#contact">聯絡我們</a>
				</li> -->
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	
	</div>
	<!-- /.container-fluid -->
</nav>