package lib;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
/**
 * Converter Date time format
 * @author nolions
 *
 */
public class FormatDateTime {
	
	/**
	 * Get date time of Zone 
	 * @param GMT
	 * @see float
	 * @return time Zone date time
	 */
	static public Date getNowTimeZoneDate(float GMT)
	{
		// Format time
		SimpleDateFormat nowdate = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
		
		// Set Time Zone
		nowdate.setTimeZone(TimeZone.getTimeZone("GMT+"+ GMT));
		//nowdate.setTimeZone(TimeZone.getTimeZone("UTC"));
		
		// Get time string
		String date_Str = nowdate.format(new java.util.Date());
		
		// time string change to time date
		Date nowDate = ConverterStringToDate(date_Str);
		
		return nowDate;
	}
	
	/**
	 * Converter String time change to Date time
	 * @param dateString
	 * @return now time
	 */
	static public Date ConverterStringToDate(String dateString)
	{
		String dateFormat = "yyyyMMddHHmmss";
		
		return ConverterStringToDate(dateString, dateFormat);
	}
	
	/**
	 * Converter String time change to Date time
	 * @param dateString
	 * @param dateFormat
	 * @return now time
	 */
	static public Date ConverterStringToDate(String dateString, String dateFormat)
	{
		Date nowDate = null;
		// Set time formatting format
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		try {
			// String time to Date Time
			nowDate = sdf.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return nowDate;
	}
	
	/**
	 * Converter Date time change to String time
	 * @param date
	 * @return Date Time String
	 */
	static public String ConverterDateToString(Date date)
	{
		String dateFormat = "yyyyMMddHHmmss";
		
		return ConverterDateToString(date, dateFormat);
	}
	
	/**
	 * Converter Date time change to String time
	 * @param date
	 * @return  Date Time String
	 */
	static public String ConverterDateToString(Date date, String dateFormat)
	{
		// Set time formatting format
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

		// Date time to String Time
		String  DateString = sdf.format(date);
		
		return DateString;
	}
}
