<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>
<div class="container-fluid">
	<div class="row">
		<s:include value="../_pageheader.jsp">
			<s:param name="title">帳號設定</s:param>
		</s:include>
	</div>
	
	<s:if test="status != 0">
		<s:if test="status == 1">
			<s:set name="alertType">success</s:set>
		</s:if>
		<s:elseif test="status == -1">
			<s:set name="alertType">warning</s:set>
		</s:elseif>
		
		<div id="alert_statusbar" class="alert alert-<s:property value="alertType"/>">
			<a href="#" class="close" data-dismiss="alert">&times;</a>
			<strong><s:property value="msg"/></strong>
		</div>
	</s:if>
	
	<form action="" method="post">
		<input type="hidden" name="hiddenVal" value="updateuerinfo">
		<input type="hidden" name="userId" value="<s:property value="userinfo.Id" />">
			
		<!-- modal-body -->
		<div class="modal-body">
			<div class="input-group">
				<div class="input-group-addon">暱稱</div>
				<input id="nickname" class="form-control" type="text" name="nickname" value="<s:property value="userinfo.nickname" />" placeholder="請輸入您的舊密碼" required="required" autofocus="autofocus">
			</div>
		</div>
		<!-- /modal-body -->
			
		<div class="modal-footer">
			<button type="submit" id="storeBtn" class="btn btn-primary btn-lg">儲存</button>
			<br><br>
			<a id="changepwdBtn" class="btn btn-danger btn-lg" href="<c:url value="/console/user/changepwd"/>">
				<i class="fa fa-save"></i> 變更密碼
			</a>
		</div>
		<!-- /modal-footer -->
	</form>
</div>
<link href="<%=AssetsUrl%>/css/console/user/setting.css" rel="stylesheet">