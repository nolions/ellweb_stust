package com.ell.action.backend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ell.models.another;
import com.ell.models.researchs;
import com.ell.models.user;
import com.opensymphony.xwork2.ActionSupport;

import DataAccess.GAE_DataStoreAccess;

@SuppressWarnings("serial")
public class ConsoleAdminTeamworkerAction extends ActionSupport{
//	private ActionContext context;
//	private HttpServletRequest request ;
//	private HttpServletResponse response;
	
	private List<another> teamWorker = new ArrayList<>();
	private List<researchs> researchsList = new ArrayList<>();
	
	private List<String> researchStrs;
	
	private another userinfo;
	
	public String hiddenVal;
	public String Id;
	public int session = -1;
	public String Name = "";
	public String eName = "";
	public String email = "";
	public String masterPaper = "";
	public String research_areas = "";
	public boolean isDisable = true;
	
	private int status = user._noAction;
	private String msg = "";
	
	public ConsoleAdminTeamworkerAction(){
		
	}
	
	public String gethiddenVal() {
		return hiddenVal;
	}
	
	public String getId() {
		return Id;
	}
	
	public int getSession() {
		return session;
	}
	
	public String getName() {
		return Name;
	}
	
	public String geteName() {
		return eName;
	}

	public String getEmail() {
		return email;
	}
	
	public String getMasterPaper() {
		return masterPaper;
	}

	public String getResearch_areas() {
		return research_areas;
	}
	
	public boolean getisDisable() {
		return isDisable;
	}
	
	/**
	 * get student data list
	 * @return List<users> Array list data of Student
	 */
	public List<another> getTeamWorker(){
		return teamWorker;
	}
	
	/**
	 * get all Research data of List
	 * @return List<researchs>
	 */
	public List<researchs> getResearchsList(){
		return researchsList;
	}
	
	/**
	 * set user's Research list
	 * @return List<String>
	 */
	public List<String> getResearchStrs(){
		return researchStrs;
	}
	
	/**
	 * get user information
	 * @return users
	 */
	public another getUserinfo(){
		return userinfo;
	}
	
	public int getStatus(){
		return status;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public void sethiddenVal(String hiddenval) {
		this.hiddenVal = hiddenval;
	}
	
	public void setId(String id) {
		this.Id = id;
	}

	public void setSession(int session) {
		this.session = session;
	}
	
	public void setName(String name) {
		Name = name;
	}

	public void seteName(String eName) {
		this.eName = eName;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setMasterPaper(String masterPaper) {
		this.masterPaper = masterPaper;
	}
	
	public void setResearch_areas(String research_areas) {
		this.research_areas = research_areas;
	}

	public void setDisable(boolean isDisable) {
		this.isDisable = isDisable;
	}
	
	/**
	 * setting data list about student 
	 * @param int count data's count
	 */
	public void setTeamWorker(int count){
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("type", "student");
		
		another model = new another(paramsMap);
		teamWorker = model.getDataList(count, "session", GAE_DataStoreAccess._Sort_Desc);
	}
	
	/**
	 * setting all Research data based on disable
	 * @param Boolean isDisable 
	 */
	public void setResearchsList(Boolean isDisable){
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("isDisable", true);
		
		researchs model = new researchs();
		researchsList = model.getDataList(paramsMap, -1);
	}
	
	/**
	 * setting User's Research list
	 * @param String ResearchJsonStr
	 */
	public void setResearchStrs(String ResearchJsonStr){
		if(ResearchJsonStr != null && !ResearchJsonStr.equals(""))
			researchStrs = lib.DataTypeParser.ConvertJSONArrayStrToArrayList(ResearchJsonStr);
	}
	
	/**
	 * setting user information data by user id
	 * @param String UserId
	 */
	public void setUserinfo(String Id){
		another model = new another(Id);
		userinfo = model.getData();
		
		//set checked research list
		this.setResearchStrs(userinfo.research_areas);
	}
	
	public void setStatus(int status){
		this.status = status;
	}
	
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * Students data list of Action
	 * @return String 
	 */
	public String list(){
		this.setTeamWorker(-1);
		
		return "list";
	}
	
	public String add(){
		if (hiddenVal != null && hiddenVal.equals("addteamworker")) {
			userinfo = new another();
			userinfo.type = another._student;
			userinfo.session = session;
			userinfo.Name = Name;
			userinfo.eName = eName;
			userinfo.email = email;
			userinfo.masterPaper = masterPaper;
			
			if (userinfo.StoreData()) {
				return "list";
			}
			
		}
		
		//get all Research list
		this.setResearchsList(true);
		
		return "add";
	}
	
	/**
	 * Edit Student's information of Action
	 * @return String 
	 */
	public String edit(){
		System.out.println(Id);
		this.setUserinfo(Id);
		
		if (hiddenVal != null && hiddenVal.equals("editteamworker")) {
			userinfo.session = session;
			userinfo.Name = Name;
			userinfo.eName = eName;
			userinfo.email = email;
			userinfo.masterPaper = masterPaper;
			
			if (userinfo.UpdateData()) {
				this.setStatus(user._success);
				this.setMsg("儲存成功...");
			}else{
				this.setStatus(user._fail);
				this.setMsg("儲存失敗...");
			}
		}
		
		//get all Research list
		this.setResearchsList(true);
		
		return "edit";
	}
	
	public String delete(){
		if (Id != null && !Id.equals("") && Id.length() > 0) {
			another model = new another(Id);
			
			if (model.DeleteData()) {
				return "list";
			}
		}
		return "delete";
	}
	
	/**
	 * setting ActionContext object
	 * @return ActionContext
	 */
//	private ActionContext setActionContext(){
//		if(context == null)
//			context = ActionContext.getContext();
//		
//		return context;
//	}
	
	/**
	 * setting HttpRequest object
	 * @return HttpServletRequest
	 */
//	private HttpServletRequest setHttpRequest(){
//		if(context == null)
//			context = setActionContext();
//		
//		if(response == null)
//			request = (HttpServletRequest) context.get(ServletActionContext.HTTP_REQUEST);
//		
//		return request;
//	}
	

	/**
	 * setting HttpServletResponse object
	 * @return HttpServletResponse
	 */
//	@SuppressWarnings("unused")
//	private HttpServletResponse setHttpResponse(){
//		if(response == null)
//			response = ServletActionContext.getResponse();
//		
//		return response;
//	}
}
