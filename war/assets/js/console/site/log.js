/**
 * 
 */
$(document).ready(function() {
	var table = $('#logTable').DataTable({
		//"pagingType": "full_numbers",
		scrollCollapse:true,
		order: [[ 1,"desc"]],
		paging: true,
		searching: false,
		"language":{
			"lengthMenu": "每頁顯示 _MENU_ 筆",
			"zeroRecords": "無法搜尋到相關資料",
			"info": "第 _PAGE_ 頁 ，共 _PAGES_ 頁",
			"infoEmpty": "",
			"infoFiltered": "(filtered from _MAX_ total records)",
			"paginate": {
				"first":"第一頁",
				"last":"最末頁",
				"next":"下一頁",
				"previous":"前一頁"
			}
		},
		fnCreatedRow: function (nRow, data, iDisplayIndex) {
			var info = $(this).DataTable().page.info();
			var page = info.page;
		    var length = info.length;
		    var index = (page * length + (iDisplayIndex +1)); 
			
			$('td:eq(0)',nRow).html(index);
        },
	});
	
	table.on('order.dt search.dt', function () {
		table.column(0, {search:'applied', order:'applied'}).nodes().each( 
			function (cell, i) {
				cell.innerHTML = i+1;
			}
		);
	}).draw();
});