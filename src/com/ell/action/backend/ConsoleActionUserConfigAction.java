package com.ell.action.backend;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.ell.models.user;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;


@SuppressWarnings("serial")
public class ConsoleActionUserConfigAction extends ActionSupport {
	private ActionContext context;
	private HttpServletRequest request ;
	
	public String hiddenVal;
	public String userId;
	public String nickname;
	
	private String oldpassword;
	private String newpassword;
	private String renewpassword;
	private int status = user._noAction;
	private String msg;
	private user userinfo;
	
	public ConsoleActionUserConfigAction(){
		
	}
	
	public String getHiddenVal() {
		return hiddenVal;
	}

	public String getUserId() {
		return userId;
	}
	
	public String getnickname() {
		return userId;
	}
	
	public String getOldpassword() {
		return oldpassword;
	}
    
    public String getNewpassword() {
		return newpassword;
	}
    
    public String getRenewpassword() {
    	return renewpassword;
	}
    
    public int getStatus() {
		return status;
	}
	
	public String getMsg() {
		return msg;
	}

	public user getUserinfo(){
    	return userinfo;
    }
	
	public void setHiddenVal(String hiddenVal) {
		this.hiddenVal = hiddenVal;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public void setnickname(String nickname) {
		this.nickname = nickname;
	}
	
	public void setOldpassword(String password) {
		if(!password.equals("") && password.length() > 0)
			this.oldpassword = lib.Encrypt.EncryptSHA1((password));
		else
			this.oldpassword = null;
	}
    
    public void setNewpassword(String password) {
    	if(!password.equals("") && password.length() > 0)
    		this.newpassword = lib.Encrypt.EncryptSHA1((password));
    	else
			this.oldpassword = null;
	}
    
    public void setRenewpassword(String password) {
    	if(!password.equals("") && password.length() > 0)
    		this.renewpassword = lib.Encrypt.EncryptSHA1((password));
    	else
			this.oldpassword = null;
	}
    
    public void setStatus(int status) {
		this.status = status;
	}

	
	public void setMsg(String msg) {
		this.msg = msg;
	}
    
    public void setUserinfo(user userinfo){
    	this.userinfo = userinfo;
    }
	
	public String execute(){
    	
    	return SUCCESS;
    }
	
	public String info(){
		if(request == null)
    		setHttpRequest();
		
		HttpSession session = request.getSession();
    	String Id =(String) session.getAttribute("id");
    	
    	user model = new user(Id);
    	userinfo = model.getData();
		
		return "info";
	}
    
	public String Setting() {
    	if(request == null)
    		setHttpRequest();
    	
    	HttpSession session = request.getSession();
    	String Id =(String) session.getAttribute("id");
    	
    	userinfo = new user(Id);
    	userinfo = userinfo.getData();
    	
    	if (hiddenVal != null && hiddenVal.equals("updateuerinfo")) {
    		if (this.userId != null && this.userId.equals(Id)) {
    			userinfo.nickname = this.nickname;
    			
    			if (userinfo.UpdateData()) {
    				this.setStatus(user._success);
    				this.setMsg("設定變更成功");
				}else{
					this.setStatus(user._fail);
					this.setMsg("設定變更失敗");
				}
			}else {
				this.setStatus(user._fail);
				this.setMsg("設定變更失敗");
			}
    	}
    	
    	return "Setting";
    }
	
    public String changepassword() {
    	if(request == null)
    		setHttpRequest();
    	
    	HttpSession session = request.getSession();
    	String Id =(String) session.getAttribute("id");
    	
    	userinfo = new user(Id);
    	userinfo = userinfo.getData();
    	
    	if (hiddenVal != null && hiddenVal.equals("changepwd")) {
    		if (this.userId != null && this.userId.equals(Id)) {
    			
    			if (this.oldpassword != null && this.newpassword != null && this.renewpassword != null) {
    				if(newpassword.equals(renewpassword)){
    					if (userinfo.checkAuthAccount(userinfo.email, oldpassword)) {
    						userinfo.password = this.newpassword;
    						if (userinfo.UpdateData()) {
    							this.setStatus(user._success);
                    			this.setMsg("密碼已順利變更，請重新登入");
							}else{
								this.setStatus(user._fail);
	                			this.setMsg("密碼儲存失敗");
							}
            	    	}else {
            	    		this.setStatus(user._fail);
                			this.setMsg("密碼錯誤，請檢查你的密碼是否正確");
    					}
    				}else{
    					this.setStatus(user._fail);
            			this.setMsg("請再此檢查您的新密碼");
    				}
    				
        		}else{
        			this.setStatus(user._fail);
        			this.setMsg("*為必填寫之項目");
        		}
    		}
    		System.out.println("Status:"+user._fail);
    			
//    		if (this.userId != null && this.userId.equals(Id)) {
//    			
//    		}else{
//    			
//    		}
    	}
    	
//    	boolean AccoundExist = false;
//    	boolean AuthAccount = false;
//    	
//    	String type = setHttpRequest().getParameter("type");
//    	
//    	if (type != null) {
//    		if (oldpassword != null && newpassword != null && renewpassword != null) {
//        		if(newpassword.equals(renewpassword)){
//        			if(request == null)
//        	    		setHttpRequest();
//        	    	
////        	    	HttpSession session = request.getSession();
////        	    	String Id =(String) session.getAttribute("id");
//        	    	
////        	    	user model = new user(Id);
//        	    	AccoundExist = userinfo.chechAccoundExistbyId(Id);
//        	    	if (AccoundExist) {
//        	    		userinfo = userinfo.getData();
//        	    		AuthAccount =userinfo.checkAuthAccount(userinfo.email, oldpassword);
//        	    		
//        	    		if(AuthAccount){
//        	    			userinfo.password = newpassword;
//        	    			userinfo.UpdateData();
//        	    			
//        	    			//change password success
//        	    			//status = user._success;
//        	    		}else{
//        	    			//password error
//        	    			//status = user._errorPW;
//        	    		}
//        			}else{
//        				//account not exist
//        				//status = user._noAccount;
//        			}
//        		}else{
//        			//check new password
//        			//status = user._reCheckPW;
//        		}
//    		}else{
//    			//data not allow null
//    			//status = user._dataIsNull;
//    		}
//		}
    	
    	return "changepassword";
    }

    public String LoginLog() {
        return SUCCESS;
    }
    
    public String ActionLog() {
        return SUCCESS;
    }
    
    /**
	 * setting HttpRequest object
	 * @return HttpServletRequest
	 */
	private HttpServletRequest setHttpRequest(){
		if(context == null)
			context = setActionContext();
		
		if(request == null)
			request = (HttpServletRequest) context.get(ServletActionContext.HTTP_REQUEST);
		
		return request;
	}
	
	/**
	 * setting ActionContext object
	 * @return ActionContext
	 */
	private ActionContext setActionContext(){
		if(context == null)
			context = ActionContext.getContext();
		
		return context;
	}
}
