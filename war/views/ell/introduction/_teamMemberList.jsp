<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!-- Team Section -->
<section id="team" class="bg-light-gray">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">研究團隊</h2>
				<h3 class="section-subheading text-muted">Team Workers.</h3>
			</div>
		</div>
		
		<div class="row">
			<s:iterator value="StudentsList">
        	<div class="col-sm-4">
        		<div class="team-member">
        			<img src="./assets/img/team/team_user.png" class="img-responsive img-circle" alt="">
					<h4><s:property value="Name"/></h4>
					<p class="text-muted"><s:label><s:property value="eName"/></s:label></p>
					<ul class="list-inline social-buttons">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href=""><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-bold"></i></a></li>
					</ul>
				</div>
			</div>
			</s:iterator>
		</div>
		
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 text-center">
				<a class="btn btn-danger" href="<c:url value="/TeamWorker" />">More <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
			</div>
		</div>
	</div>
</section>