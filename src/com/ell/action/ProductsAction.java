package com.ell.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ell.models.mappingResearchsProducts;
import com.ell.models.products;
import com.ell.models.researchs;
import com.opensymphony.xwork2.ActionSupport;

import DataAccess.GAE_DataStoreAccess;

@SuppressWarnings("serial")
public class ProductsAction extends ActionSupport {
	private String research = "all";
	private String type = "all";
	private List<researchs> ResearchsList = new ArrayList<researchs>();
	private List<products> ProductsList = new ArrayList<>();
	
	public String getResearch(){
		return research;
	}
	
	public String getType(){
		return type;
	}
	
	public List<researchs> getResearchsList(){
		return ResearchsList;
	}
	
	public List<products> getProductsList(){
		return ProductsList;
	}
	
	public void setResearch(String research){
		this.research = research;
	}
	
	public void setType(String type){
		this.type = type;
	}
	
	public void setResearchsList(){
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("isDisable", true);
		
		researchs model = new researchs();
		ResearchsList = model.getDataList(paramsMap, -1);
	}
	
	public void setProductsList(String research, String type){
		List<products> tmpProductsList = new ArrayList<>();
		if (research.equals("all")) {
			HashMap<String, Object> paramsMap = new HashMap<String, Object>();
			products model = new products();
			tmpProductsList = model.getDataList(paramsMap, -1, "from", GAE_DataStoreAccess._Sort_Desc);
		}else{
			researchs r = new researchs();
			String rId = r.getData("Name", research).Id;
			
			mappingResearchsProducts mrp = new mappingResearchsProducts();
			HashMap<String, Object> mrpParamsMap = new HashMap<String, Object>();
			mrpParamsMap.put("researchID", rId);
			List<mappingResearchsProducts> mrpList = mrp.getDataList(mrpParamsMap);
			
			for (mappingResearchsProducts model : mrpList) {
				products pModel = new products();
				tmpProductsList.add(pModel.getData(model.productsID));
			}
		}
		
		if (!type.equals("all")) {
			for(products product: tmpProductsList){
				if(product.type.equals(type)){
					ProductsList.add(product);
				}
			}
		}else {
			ProductsList = tmpProductsList;
		}
	}
	
	public String execute() {
		this.setResearchsList();
		this.setProductsList(this.research, this.type);
		
		return "SUCCESS";
	}
}
