<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<ol class="breadcrumb" style="background-color:#fff; padding-left:0; padding-right:0;">
				<li><a href="/console"><i class="fa fa-home"></i> Home</a></li>
				<s:include value="../_breadcrumbItem.jsp">
					<s:param name="label">研究方向</s:param>
				</s:include>
			</ol>
			<h2 class="page-header" style="margin-top:10px;">研究方向</h2>
		</div>
	</div>
	
	<div id="researchs_container" class="content_container">
		<div class="">
			<a class="btn btn-primary" href="<c:url value="/console/Research/add"/>" role="button">
				<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 新增
			</a>
		</div>
		<br>
	
		<div class="list-group">
		<s:iterator value="researchsList" var="research">
			<div id="<s:property value="Id"/>_media" class="media list-group-item">
						<div class="media-left">
							<img alt="64x64" class="img-thumbnail media-object" data-src="holder.js/64x64" style="width: 100px; height: 100px;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTYzMTc3NTgyMCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1NjMxNzc1ODIwIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy40Njg3NSIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4=" data-holder-rendered="true">
						</div>
						<div class="media-body">
							<h4 class="media-heading"><s:property value="Name"/> | <s:property value="eName"/></h4>
							<s:property value="Descride"/>
							<br><br>
							<s:property value="Source"/>，<s:property value="ReadTime"/>編輯
						</div>
						
						<div class="media-right">
							<a class="btn btn-default" href="<c:url value="/console/Research/edit?Id=${Id}"/>">編輯</a>
						<br><br>
						<a class="btn btn-danger" href="<c:url value="/console/Research/delete?Id=${Id}"/>">刪除</a>
				</div>
				</div>
		</s:iterator>
		</div>
	</div>
</div>

<!-- moment -->
<%-- <script src="<%=AssetsUrl%>/product/moment.js"></script> --%>

<!-- Bootstrap datetimepicker -->
<%-- <link rel="stylesheet" href="<%=AssetsUrl%>/product/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css">
<script src="<%=AssetsUrl%>/product/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script> --%>

<%-- <link rel="stylesheet" href="<%=AssetsUrl%>/product/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css">
<script src="<%=AssetsUrl%>/product/bootstrap-switch/js/bootstrap-switch.min.js"></script> --%>