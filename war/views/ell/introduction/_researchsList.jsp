<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!-- Researchs Section -->
<section id="researchs" class="bg-light-gray">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">研究方向</h2>
				<h3 class="section-subheading text-muted">Research Direction.</h3>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<ul class="timeline">
					<%int i =0; %>
					<s:iterator value="ResearchsList">
					<li class="<%=(i%2==0)?"":"timeline-inverted"%>">
						<div class="timeline-image">
							<img class="img-circle img-responsive" src="./assets/img/about/1.png" alt="">
						</div>
						
						<div class="timeline-panel">
							<div class="timeline-heading">
								<h4><s:property value="Name"/></h4>
								<h4 class="subheading"><s:property value="eName"/></h4>
							</div>
							
							<div class="timeline-body">
								<p class="text-muted">
									<s:property value="@lib.DataTypeParser@ConvertStringCount(Descride, 100)"/>
									<br>
									<s:date name="ReadTime" format="yyyy/MM/dd" />
								</p>
							</div>
							<a class="btn btn-success" href="<s:property value="Source"/>" target="_blank">
								參考來源 <span class="glyphicon glyphicon-link" aria-hidden="true"></span>
							</a>
							<a class="btn btn-info" href="<c:url value="/Products?research=${Name}&type=all"/>">
								相關研究 <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
							</a>
						</div>
					</li>
					<% i++; %>
					</s:iterator>
				</ul>
			</div>
		</div>
	</div>
</section>