package com.ell.action.backend;

import java.util.ArrayList;
import java.util.HashMap;

import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class ConsoleSiteAction extends ActionSupport {
	public ArrayList<HashMap<String, String>> devlogList;
	public ArrayList<HashMap<String, String>> getDevlogList() {
		return devlogList;
	}
	
	public void setDevlogList() {
		devlogList = new ArrayList<>(); 
		devlogList.add(addLogMap("2017/11/05", "bug fix: product and teamwork data sort"));
		devlogList.add(addLogMap("2017/11/04", "query product by research and type"));
		devlogList.add(addLogMap("2017/11/02", "add dev log on backend"));
		devlogList.add(addLogMap("2017/11/01", "Another mapping to Products"));
		devlogList.add(addLogMap("2017/11/01", "Research mapping to Products"));
		devlogList.add(addLogMap("2017/10/31", "update breadcrumb on backend"));
		devlogList.add(addLogMap("2017/10/30", "url rewrite by urlrewrite JAR Libaray"));
		devlogList.add(addLogMap("2017/09/09", "integrate update backend UI"));
		devlogList.add(addLogMap("2017/09/09", "integrate ellweb_stust(fortend) and backend_ellweb_stust(backend) to a project"));
	}
	
	public String log() {
		this.setDevlogList();
        return "log";
	}
	
	private HashMap<String, String> addLogMap(String date, String log){
		HashMap<String, String> map  = new HashMap<>();
		map.put("date", date);
		map.put("log", log);
		
		return map;
	}
}
