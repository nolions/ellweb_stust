package com.ell.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.google.appengine.api.datastore.Entity;

import DataAccess.GAE_DataStoreAccess;
import lib.FormatDateTime;

public class researchs{
	public String Id;
	public String Name;
	public String eName;
	public String Descride;
	public String Source;
	public Date ReadTime;
	public Date CreateTime;
	public Date UpdateTime;
	public boolean isDisable = true;
	
	private String dbTable = "researchs";
	
	public researchs(){
		Id = UUID.randomUUID().toString();
		CreateTime = FormatDateTime.getNowTimeZoneDate(0);
		UpdateTime = CreateTime;
		ReadTime = CreateTime;
	}
	
	public researchs(String Id){
		this.Id = Id;
	}
	
	public researchs getData(){
		GAE_DataStoreAccess mode = new GAE_DataStoreAccess(dbTable);
		Entity entity = mode.getData(this.Id);
		
		return EntityToModel(entity);
	}
	
	public researchs getData(String key, String value){
		GAE_DataStoreAccess mode = new GAE_DataStoreAccess(dbTable);
		Entity entity = mode.getData(key, value);
		
		return EntityToModel(entity);
	}
	
	public List<researchs> getDataList(HashMap<String, Object> paramsMap, int limit){
		GAE_DataStoreAccess mode = new GAE_DataStoreAccess(dbTable);
		List<Entity> entityList = mode.getDataList(paramsMap, limit);
		
		List<researchs> data = new ArrayList<>();
		for (Entity entity : entityList) {
			researchs model = EntityToModel(entity);
			data.add(model);
		}
		
		return data;
	}
	
	public boolean StoreData(){
		Map<String, Object> dataList = ModeltoMapList(this);
		
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		return gaeDSAmodel.Save(dataList);
	}
	
	public boolean BatchStoreData(ArrayList<researchs> modelList){
		ArrayList<Map<String, Object>> dataList = new ArrayList<>();
		
		for (int i = 0; i < modelList.size(); i++) {
			researchs model = modelList.get(i);
			
			Map<String, Object> map = ModeltoMapList(model);
			
			dataList.add(map);
		}
		
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		return gaeDSAmodel.BatchSave(dataList);
	}
	
	public boolean UpdateData(){
		Map<String, Object> dataList = ModeltoMapList(this);
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		
		return gaeDSAmodel.Update(this.Id, dataList);
	}
	
	public boolean DeleteData(){
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		return gaeDSAmodel.Delete(this.Id);
	}
	
	static public Map<String, Object> ModeltoMapList(researchs model){
		Map<String, Object> dataMap = new HashMap<>();
		dataMap.put("Id", model.Id);
		dataMap.put("Name", model.Name);
		dataMap.put("eName", model.eName);
		dataMap.put("isDisable", model.isDisable);
		
		dataMap.put("CreateTime", model.CreateTime);
		dataMap.put("UpdateTime", model.UpdateTime);
		
		if (model.Descride != null) dataMap.put("Descride", model.Descride);
		if (model.Source != null) dataMap.put("Source", model.Source);
		
		if (model.ReadTime != null) dataMap.put("ReadTime", model.ReadTime);
		
		return dataMap;
	}
	
	static private researchs EntityToModel(Entity entity){
		researchs model = new researchs();
		model.Id = entity.getProperty("Id").toString();
		model.Name = entity.getProperty("Name").toString();
		model.eName = (entity.getProperty("eName") == null)?null:entity.getProperty("eName").toString();
		model.Descride = (entity.getProperty("Descride") == null)?null:entity.getProperty("Descride").toString();
		model.Source = (entity.getProperty("Source") == null)?null:entity.getProperty("Source").toString();
		model.isDisable = (boolean) entity.getProperty("isDisable");
		model.ReadTime = (Date) entity.getProperty("ReadTime");
		model.CreateTime = (Date) entity.getProperty("CreateTime");
		model.UpdateTime = 	(Date) entity.getProperty("UpdateTime");
		
		
		return model;
	}
	
	static public researchs MapListtoModel(Map<String, Object> map){
		researchs model = new researchs();
		if (map.get("Name") != null) 
			model.Name = map.get("Name").toString();
		
		if (map.get("eName") != null) 
			model.eName = map.get("eName").toString();
		
		if (map.get("Descride") != null) 
			model.Descride = map.get("Descride").toString();
		
		if (map.get("Source") != null) 
			model.Source = map.get("Source").toString();
		
		return model;
	}
	
	static public researchs MapListtoModel(Map<String, Object> map, researchs model){
		if (map.get("Name") != null) 
			model.Name = map.get("Name").toString();
		
		if (map.get("eName") != null) 
			model.eName = map.get("eName").toString();
		
		if (map.get("Descride") != null) 
			model.Descride = map.get("Descride").toString();
		
		if (map.get("Source") != null) 
			model.Source = map.get("Source").toString();
		
		model.UpdateTime = FormatDateTime.getNowTimeZoneDate(0);
		
		return model;
	}
}
