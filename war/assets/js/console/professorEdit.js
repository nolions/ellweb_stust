$(document).ready(function() {
	$('.close-btn').click(function(){
		closefancybox(false);
	});	
	
	$('.sendUpdate-btn').click(function(){
		//$(this).attr('disabled', true);
		$("button").attr('disabled', true);
		
		var checkedValue = $('input[name="research"]:checked').map(function(){
			return $(this).val();
		}).get().join(',');
		
		console.log('userid:'+  $('#editUserId').val());
		console.log('job:'+  $('#editJob').val());
		console.log('username:'+  $('#editName').val());
		console.log('userename:'+  $('#editEName').val());
		console.log('education:'+  $('#editEducation').val());
		console.log('email:'+  $('#editEmail').val());
		console.log('tel:'+  $('#editTel').val());
		console.log('lab:'+  $('#editLab').val());
		console.log('officetime:'+  $('#editOfficeTime').val())
		console.log('research:'+ checkedValue);
		
		$.ajax({
			url: '/api/user.servlet',
			data: {
				'action': 'updateAdvisor',
				'userid' : $('#editUserId').val(),
				'job' : $('#editJob').val(),
				'username' : $('#editName').val(),
				'userename' : $('#editEName').val(),
				'education' : $('#editEducation').val(),
				'email' : $('#editEmail').val(),
				'tel' : $('#editTel').val(),
				'lab' : $('#editLab').val(),
				'officetime' : $('#editOfficeTime').val(),
				'research' : checkedValue,
			},
			type:"POST",
			dataType:'json',
			success: function(msg){
				//var status = JSON.stringify(msg);//JSON.parse(msg);
				console.log(msg.status);
				
				var StatusInfoText = "";
				
				if(msg.status)
					StatusInfoText = "儲存成功......該狀態列將於5秒後自動關閉";
				else
					StatusInfoTextg = "儲存失敗......該狀態列將於5秒後自動關閉";
					
				ShowStatusBar(StatusInfoText, msg.status);
				
			},error:function(xhr, ajaxOptions, thrownError){ 
				console.log(xhr.status);
				console.log(thrownError);
				
				StatusInfoTextg = "儲存失敗......該狀態列將於5秒後自動關閉";
				ShowStatusBar(StatusInfoTextg, false);
			}
		});
		
		return false;
	});
	
	$('.delete-btn').click(function(){
		$("button").attr('disabled', true);
		
		var checkedValue = $('input[name="research"]:checked').map(function(){
			return $(this).val();
		}).get().join(',');
		console.log(checkedValue);
		
		$.ajax({
			url: '/api/user.servlet',
			data: {
				'action': 'delete',
				'userid' : $('#editUserId').val(),
			},
			type:"POST",
			dataType:'json',
			success: function(msg){
				//var status = JSON.stringify(msg);//JSON.parse(msg);
				console.log(msg.status);
				
				var StatusInfoText = "";
				
				if(msg.status)
					StatusInfoText = "刪除中，請稍候......該狀態列將於刪除完成後5秒後自動關閉";
				else
					StatusInfoText = "刪除中失敗......該狀態列將於5秒後自動關閉";
					
				ShowStatusBar(StatusInfoText, msg.status);
				
			},error:function(xhr, ajaxOptions, thrownError){ 
				console.log(xhr.status);
				console.log(thrownError);
				
				StatusInfoTextg = "刪除中失敗......該狀態列將於5秒後自動關閉";
				ShowStatusBar(StatusInfoTextg, false);
			}
		});
		
		return false;
	});
	
	function ShowStatusBar(msg, isStatus){
		console.log(msg);
		$(".statusBar").text(msg);
		
		if(isStatus){
			$(".statusBar").removeClass('bg-warning').addClass('bg-danger');
		}else{
			$(".statusBar").removeClass('bg-danger').addClass('bg-warning');
		
		}$(".statusBar").show();
		
		setTimeout(updateDone, 5000);
	}
	
	function sleep(sec) {
	    var time = new Date().getTime();
	    while(new Date().getTime() - time < sec * 1000);
	}
	
	function updateDone(){
		$(".statusBar").hide();
		closefancybox(true);
	}
	
	function closefancybox(isReload){
		parent.$.fancybox.close();
		
		if(isReload)
			parent.location.reload(true);
	}
});