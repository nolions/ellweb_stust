<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!-- Professor Section -->
<section id="professor">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">指導教授</h2>
				<h3 class="section-subheading text-muted">Advisor.</h3>
			</div>
		</div>

		<!-- Project One -->
		<div class="row">
		<s:iterator value="AdvisorList">
			<div class="col-md-4">
	        	<a href="#">
	        		<img class="img-responsive img-thumbnail" src="../assets/img/team/cjz.jpg" alt="">
				</a>
			</div>
		
			<div class="col-md-8">
				<h3><s:property value="Name"/> 教授</h3>
				<h4><s:property value="eName"/></h4>
				<p>
					<span class="glyphicon glyphicon-education bold"> 學歷：</span>
					<s:property value="education"/>
				</p>
				<p>
					<span class="glyphicon glyphicon-inbox bold"> 專長及研究領域：</span>
					<s:property value="researchToString(research_areas)"/>
					<%-- <s:property value="research_areas"/> --%>
				</p>
				<p>
					<span class="glyphicon glyphicon-home bold"> 研究室：</span> 
					<s:property value="lab"/>
				</p>
				<p>
					<span class="glyphicon glyphicon-phone-alt bold"> 分機號碼：</span>
					<s:property value="tel"/>
				</p>
				<p>
					<span class="glyphicon glyphicon-envelope bold"> E-Mail：</span>
					<a href="mailto:#"><s:property value="email"/></a>
				</p>
				<a class="btn btn-danger" href="<c:url value="/Professor?Id=${Id}"/>">
					More
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				</a>
			</div>
		</s:iterator>
		</div>		
	</div>
</section>