<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>
<!DOCTYPE html>
<html lang="zn-tw">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>SDaBmin - Ell Lab for STUST Admin</title>

	<%-- <link rel="Shortcut Icon" type="image/x-icon" href="../<%=RootUrl %>favicon.png" /> --%>
	
    <!-- Bootstrap Core CSS -->
    <link type="text/css" href="<%=AssetsUrl%>/product/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    
    <!-- MetisMenu CSS -->
    <link type="text/css" href="<%=AssetsUrl%>/product/metisMenu/css/metisMenu.min.css" rel="stylesheet" media="screen">
    
    <!-- fancyBox CSS -->
	<link type="text/css" href="<%=AssetsUrl%>/product/fancybox/jquery.fancybox.css?v=2.1.5" rel="stylesheet" media="screen" />

    <!-- Custom CSS -->
    <link type="text/css" href="<%=AssetsUrl%>/theme/sbadmin2/css/sb-admin-2.css" rel="stylesheet" media="screen">

    <!-- Custom Fonts -->
    <link type="text/css" href="<%=AssetsUrl%>/product/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen">

	<!-- jQuery -->
	<script type="text/javascript" src="<%=AssetsUrl%>/product/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script type="text/javascript" src="<%=AssetsUrl%>/product/bootstrap/js/bootstrap.min.js"></script>
	
	<!-- Metis Menu Plugin JavaScript -->
    <script type="text/javascript" src="<%=AssetsUrl%>/product/metisMenu/js/metisMenu.min.js"></script>
    
	<!-- mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="<%=AssetsUrl%>/product/jquery.mousewheel-3.0.6.pack.js"></script>

	<!-- fancyBox main JS -->
	<script type="text/javascript" src="<%=AssetsUrl%>/product/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
	
    <!-- Custom Theme JavaScript -->
    <script type="text/javascript" src="<%=AssetsUrl%>/theme/sbadmin2/js/sb-admin-2.js"></script>
    
    <script type="text/javascript" src="<%=AssetsUrl%>/js/lightbox_style.js"></script>
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<!-- analyticstracking -->
	<tiles:insertAttribute name="analyticstracking" />
	<!-- end analyticstracking -->
</head>
<body>
<div id="wrapper">

	<!-- Navigation -->
	<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
	
		<!-- topbar header -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/console">ellSdBackend</a>
		</div>
		<!-- end topbar header -->
	
		<!-- topbar notifical  -->
		<tiles:insertAttribute name="notifical" />
		<!-- end topbar notifical -->
	
		<!-- slider menu -->
		<tiles:insertAttribute name="sildermenu" />
		<!-- end slider menu -->
	</nav>
	<!-- end Navigation -->
       
	<!-- Page Content -->
	<div id="page-wrapper">
		<tiles:insertAttribute name="body" />
	</div>
	<!-- end Page Content -->
</div>

</body>

</html>
