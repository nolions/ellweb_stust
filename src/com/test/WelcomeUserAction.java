package com.test;

import com.opensymphony.xwork2.ActionSupport;

public class WelcomeUserAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String execute() {
		return "SUCCESS";
	}
	
	public String tiger() { return "tiger"; }
	   public String lion() { return "lion"; } 
}