<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String homeUrl = "https://ell-web-of-stust.appspot.com";
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>

<div id="about-link" class="bg-darkest-gray" style="">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="about-link-block" style="">
					<div class="header-title">Ell Lab</div>
					<ul class="footer-ul">
						<li><a class="link-text-muted" href="/">網站首頁</a></li>
						<li><a class="link-text-muted" href="/#about">關於我們</a></li>
						<li><a class="link-text-muted" href="<c:url value="/TeamWorker"/>">研究團隊</a></li>
						<li><a class="link-text-muted" href="<c:url value="/Products"/>">研究成果</a></li>
						<!-- <li><a class="link-text-muted" href="/">聯絡我們</a></li> -->
					</ul>
				</div>
				<div class="about-link-block" style="">
					<div class="header-title">南臺科大</div>
					<ul class="footer-ul">
						<li><a class="link-text-muted" href="http://www.stust.edu.tw/" target="_blank">南臺科技大學</a></li>
						<li><a class="link-text-muted" href="http://business.stust.edu.tw/" target="_blank">南臺商管學院</a></li>
						<li><a class="link-text-muted" href="http://mis.stust.edu.tw/" target="_blank">南臺資管系(所)</a></li>
						<li><a class="link-text-muted" href="https://www.facebook.com/stust.edu.tw" target="_blank">南臺科大粉絲團</a></li>
						<li><a class="link-text-muted" href="https://www.facebook.com/groups/321473474586167/" target="_blank">資管學系粉絲團</a></li>
						<li><a class="link-text-muted" href="https://www.facebook.com/groups/377430735649418/" target="_blank">數位學習研究室粉絲團</a></li>
					</ul>
				</div>
				<div class="about-link-block">
				</div>
				<div id="QRCode_div" style="width:200px; float:right;">
					
						<%-- <img id="qrcode-image" src="<%=AssetsUrl%>/img/qrcode.png" /> --%>
						<img id="qrcode-image" src="<%=lib.Chart.gteQRCodeAPI(homeUrl, "165x165")%>" />
					
					
				</div>
			</div>
		</div>
	</div>
</div>

<footer class="bg-darkest-gray" style="">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<span class="copyright text-muted">
					Copyright &copy; E-Learning Lab of STUST 
					<%=new Date().getYear()+1900 %>
				</span>
				<br>
				<span class="copyright text-muted">
					為了讓你可以獲取最佳最佳網頁視覺效果，建議使用Chrome或Firefox瀏覽器，並使用1064*768以上解析度。
				</span>
			</div>
		</div>
	</div>
</footer>