package com.ell.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query.SortDirection;

import DataAccess.GAE_DataStoreAccess;
import lib.DataTypeParser;
import lib.FormatDateTime;

public class another{
	static public String _professor = "professor";
	static public String _student = "student";
	
	public String Id;
	public String Name = "";
	public String eName = "";
	public String email = "";
	public String type = _student;
	public String job = "";
	public String education = "";
	public int session = -1;
	public String lab = "";
	public String tel = "";
	public String officetime = "";
	public String research_areas = "";
	public String masterPaper = "";
	public Date CreateTime;
	public Date UpdateTime;
	public boolean isDisable = true;
	
	private String dbTable = "another";
	private HashMap<String, Object> paramsMap;
	
	public another(){
		Id = UUID.randomUUID().toString();
		CreateTime = FormatDateTime.getNowTimeZoneDate(0);
		UpdateTime = CreateTime;
	}

	public another(String id){
		this.Id = id;
	}
	
	public another(HashMap<String, Object> paramsMap){
		this.paramsMap = paramsMap;
	}
	
	public another getData(){
		GAE_DataStoreAccess mode = new GAE_DataStoreAccess(dbTable);
		Entity entity = mode.getData(this.Id);
		
		return EntityToModel(entity);
	}
	
	public List<another> getDataList(int limit) {
		return this.getDataList(limit, null, null);
	}
	
	public List<another> getDataList(int limit, String columnName, SortDirection sort) {
		GAE_DataStoreAccess mode = new GAE_DataStoreAccess(dbTable);
		List<Entity> entityList = mode.getDataList(this.paramsMap, limit, columnName, sort);
		
		List<another> data = new ArrayList<>();
		for (Entity entity : entityList) {
			another model = EntityToModel(entity);
			data.add(model);
		}
		
		return data;
	}
	
	public boolean StoreData(){
		Map<String, Object> dataList = ModeltoMapList(this);
		
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		return gaeDSAmodel.Save(dataList);
	}
	
	public boolean BatchStoreData(ArrayList<another> modelList){
		ArrayList<Map<String, Object>> dataList = new ArrayList<>();
		
		for (int i = 0; i < modelList.size(); i++) {
			another model = modelList.get(i);
			
			Map<String, Object> map = ModeltoMapList(model);
			
			dataList.add(map);
		}
		
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		return gaeDSAmodel.BatchSave(dataList);
	}
	
	public boolean UpdateData(){
		Map<String, Object> dataList = ModeltoMapList(this);
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		
		return gaeDSAmodel.Update(this.Id, dataList);
	}
	
	public boolean DeleteData(){
		GAE_DataStoreAccess gaeDSAmodel = new GAE_DataStoreAccess(this.dbTable);
		return gaeDSAmodel.Delete(this.Id);
	}
	
	static private another EntityToModel(Entity entity){
		another model = new another();
		model.Id = entity.getProperty("Id").toString();
		model.Name = entity.getProperty("name").toString();
		model.eName = (entity.getProperty("eName") != null)?entity.getProperty("eName").toString():"";
		model.email = (entity.getProperty("email") != null)?entity.getProperty("email").toString():"";
		model.education = (entity.getProperty("education") != null)?entity.getProperty("education").toString():"";
		model.tel = (entity.getProperty("tel") != null)?entity.getProperty("tel").toString():"";
		model.lab = (entity.getProperty("lab") != null)?entity.getProperty("lab").toString():"";
		model.type = (entity.getProperty("type") != null)?entity.getProperty("type").toString():"";
		model.job = (entity.getProperty("job") != null)?entity.getProperty("job").toString():"";
		model.officetime = (entity.getProperty("officetime") != null)?entity.getProperty("officetime").toString():"";
		model.session = Integer.parseInt(entity.getProperty("session").toString());
		model.research_areas = (entity.getProperty("research_areas") != null)?entity.getProperty("research_areas").toString():"";
		model.masterPaper = (entity.getProperty("masterPaper") != null)?entity.getProperty("masterPaper").toString():"";
		model.isDisable = (boolean) entity.getProperty("isDisable");
		model.CreateTime = (Date) entity.getProperty("CreateTime");
		model.UpdateTime = 	(Date) entity.getProperty("UpdateTime");
		
		return model;
	}
	
	/**
	 * Build experience data Map List. Converter experience model data to Map's ArrayList
	 * @param model experience data model
	 * @return experience data's Map list
	 */
	static public Map<String, Object> ModeltoMapList(another model){
		Map<String, Object> dataMap = new HashMap<>();
		dataMap.put("Id", model.Id);
		dataMap.put("name", model.Name);
		dataMap.put("type", model.type);
		dataMap.put("isDisable", model.isDisable);
		
		dataMap.put("CreateTime", model.CreateTime);
		dataMap.put("UpdateTime", model.UpdateTime);
		
		if (model.eName != null) dataMap.put("eName", model.eName);
		if (model.education != null) dataMap.put("education", model.education);
		if (model.email != null) dataMap.put("email", model.email);
		if (model.tel != null) dataMap.put("tel", model.tel);
		if (model.lab != null) dataMap.put("lab", model.lab);
		if (model.job != null) dataMap.put("job", model.job);
		if (model.officetime != null) dataMap.put("officetime", model.officetime);
		if (model.session != 0) dataMap.put("session", model.session);
		if (model.research_areas != null) dataMap.put("research_areas", model.research_areas);	
		if (model.masterPaper != null) dataMap.put("masterPaper", model.masterPaper);	
		
		return dataMap;
	}
	
	static public another MapListtoModel(Map<String, Object> map){
		another model = new another();
		
		if (map.get("Name") != null) 
			model.Name = map.get("Name").toString();
		if (map.get("eName") != null) 
			model.eName = map.get("eName").toString();
		
		if (map.get("type") != null) 
			model.type = map.get("type").toString();
		
		if (model.type.equals(_professor)) {
			if (map.get("tel") != null) 
				model.tel = map.get("tel").toString();
			
			if (map.get("lab") != null) 
				model.lab = map.get("lab").toString();
			
			if (map.get("officetime") != null) 
				model.officetime = map.get("officetime").toString();
		}else if (model.type.equals(_student)) {
			if (map.get("session") != null) 
				model.session = Integer.valueOf(map.get("session").toString());
			
			if (map.get("masterPaper") != null) 
				model.masterPaper = map.get("masterPaper").toString();
		}
		
		if (map.get("email") != null) 
			model.email = map.get("email").toString();
		
		if (map.get("education") != null) 
			model.education = map.get("education").toString();
		
		if (map.get("research_areas") != null) 
			model.research_areas = map.get("research_areas").toString();
		
		if (map.get("job") != null) 
			model.job = map.get("job").toString();

		return model;
	}
	
	static public another MapListtoModel(Map<String, Object> map, another model){
		if (map.get("Name") != null) 
			model.Name = map.get("Name").toString();
		if (map.get("eName") != null) 
			model.eName = map.get("eName").toString();
		
		if (map.get("type") != null) 
			model.type = map.get("type").toString();
		
		if (model.type.equals(_professor)) {
			if (map.get("tel") != null) 
				model.tel = map.get("tel").toString();
			
			if (map.get("lab") != null) 
				model.lab = map.get("lab").toString();
			
			if (map.get("officetime") != null) 
				model.officetime = map.get("officetime").toString();
		}else if (model.type.equals(_student)) {
			if (map.get("session") != null) 
				model.session = Integer.valueOf(map.get("session").toString());
			
			if (map.get("masterPaper") != null) 
				model.masterPaper = map.get("masterPaper").toString();
		}
		
		if (map.get("email") != null) 
			model.email = map.get("email").toString();
		
		if (map.get("education") != null) 
			model.education = map.get("education").toString();
		
		if (map.get("research_areas") != null) 
			model.research_areas = map.get("research_areas").toString();
		
		if (map.get("job") != null) 
			model.job = map.get("job").toString();

		model.UpdateTime = FormatDateTime.getNowTimeZoneDate(0);
		
		return model;
	}
	
	static public String toResearchsListStr(String researchsJSONArrayStr){
		ArrayList<String> researchsList = DataTypeParser.ConvertJSONArrayStrToArrayList(researchsJSONArrayStr);
		
		String researchsStr ="";
		
		for(int i=0; i<researchsList.size(); i++){
			String areaStr = researchsList.get(i);
			if(researchsStr.length()>0)
				researchsStr = researchsStr +"、"+ areaStr;
			else
				researchsStr = areaStr;
		}
		
		return researchsStr;
	}
}
