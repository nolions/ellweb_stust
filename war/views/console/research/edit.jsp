<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>	
<%
String RootUrl = request.getContextPath();
String AssetsUrl = request.getContextPath()+"/assets";
%>

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<ol class="breadcrumb" style="background-color:#fff; padding-left:0; padding-right:0;">
				<li><a href="/console"><i class="fa fa-home"></i> Home</a></li>
				<s:include value="../_breadcrumbItem.jsp">
					<s:param name="link">/console/Research/list</s:param>
					<s:param name="label">研究方向</s:param>
				</s:include>
				<s:include value="../_breadcrumbItem.jsp">
					<s:param name="label">編輯研究方向 - <s:property value="research.Name" /></s:param>
				</s:include>
			</ol>
			<h2 class="page-header" style="margin-top:10px;">編輯研究方向 - <s:property value="research.Name" /></h2>
		</div>
	</div>
	
	<s:if test="status != 0">
		<s:if test="status == 1">
			<s:set name="alertType">success</s:set>
		</s:if>
		<s:elseif test="status == -1">
			<s:set name="alertType">warning</s:set>
		</s:elseif>
		
		<div id="alert_statusbar" class="alert alert-<s:property value="alertType"/>">
			<a href="#" class="close" data-dismiss="alert">&times;</a>
			<strong><s:property value="msg"/></strong>
		</div>
	</s:if>
	
	<div id="advisor_container" class="content_container">
		<form action="" method="post">
			<input type="hidden" name="hiddenVal" value="editresearch">
			
			<!-- modal-body -->
			<div class="modal-body">
				<div class="alert alert-danger hide">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  					<span class="sr-only">Error:</span>
					<label id="alertMsg"></label>
				</div>
			
				<div class="input-group">
					<div class="input-group-addon">研究名稱</div>
					<input type="text" id="Name" class="form-control" name="Name" value="<s:property value="research.Name" />" placeholder="請輸入研究方向">
				</div>
				<div class="input-group">
					<div class="input-group-addon">英文名稱</div>
					<input type="text" id="eName" class="form-control" name="eName" value="<s:property value="research.eName" />" placeholder="請輸入研究方向英文名稱">
				</div>
				<div class="input-group">
					<div class="input-group-addon">參考來源</div>
					<input type="text" id="Source" class="form-control" name="Source" value="<s:property value="research.Source" />" placeholder="請輸入研究方向參考文獻網址">
				</div>
				<div class="input-group">
					<div class="input-group-addon">參考日期</div>
					<input type="date" id="ReadTime" class="form-control" name="ReadTime" value="<s:date name="research.ReadTime" format="yyyy-MM-dd" />" placeholder="2014/09/18" >
					<div class="input-group-addon">
                       	<span class="glyphicon glyphicon-calendar"></span>
                   	</div>
				</div>
				<div class="input-group">
					<div class="input-group-addon">描述說明</div>
					<textarea id="Descride" name="Descride" rows="10" cols="55" style="width:100%;"><s:property value="research.Descride"/></textarea>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary sendUpdate-btn"><i class="fa fa-save"></i> 儲存</button>
			</div>
			<!-- /modal-footer -->
		</form>
	</div>
</div>

<link href="<%=AssetsUrl%>/css/console/form.css" rel="stylesheet">