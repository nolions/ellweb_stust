package com.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ell.models.user;

public class userServlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String returnStr ="";
		String mActiom = req.getParameter("action");
		if(mActiom!=null && !mActiom.isEmpty()){
		    switch (mActiom) {
		    	case "CreateUser":
		    		boolean isCreateUser = false;
		    		
		    		if (req.getParameter("email") != null)
		    			isCreateUser = CreateUserInfo(req);
		    		
		    		returnStr = lib.DataTypeParser.EncodeJSONObject("status", isCreateUser).toString();
		    		break;
		    	case "UpdateUserInfo":
		    		boolean isUpdateUserInfo = UpdateUserInfo(req);
					
					returnStr = lib.DataTypeParser.EncodeJSONObject("status", isUpdateUserInfo).toString();
					break;
		    	default:
					break;
		    }
		}
		
		resp.setContentType("text/plain");
		resp.setCharacterEncoding("UTF-8");
		
		resp.getWriter().println(returnStr);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		doGet(req, resp);
	}
	
	private boolean CreateUserInfo(HttpServletRequest req){
		System.out.println("com.servlet.userServlet::CreateUserInfo()");
		
		boolean isStore = false;
		
		user model = new user();
		model.email = req.getParameter("email");
		
		if (req.getParameter("password") != null)
			model.password = lib.Encrypt.EncryptSHA1(req.getParameter("password"));
		
		if (req.getParameter("nickname") != null)
			model.nickname = req.getParameter("nickname");
		
		isStore = model.StoreData();
		return isStore;
	}
	
	private boolean UpdateUserInfo(HttpServletRequest req){
		System.out.println("com.servlet.userServlet::UpdateUserInfo()");
		
		boolean isUpdate = true;
		
		String mUserid = req.getParameter("userid");
		//user model = user.getUserInfo(mUserid);
		user model = new user(mUserid);
		model = model.getData();
		
		//update users model data
		model.nickname = (req.getParameter("nickname")!= null && !req.getParameter("nickname").isEmpty())?req.getParameter("nickname"):"";
		
		try {
			//isUpdate = user.updateUserInfo(model);
			isUpdate = model.UpdateData();
		} catch (Exception e) {
			System.out.println("com.servlet.usersServlet.UpdateUserInfo(), Exception = " + e.getMessage());
			isUpdate = false;
		}
		
		return isUpdate;
	}
}
