$(document).ready(function() {
	$('#addbtn').click(function(){
		var resultType = $('#resultType').val();
		var resultTitle = $('#resultTitle').val();
		
		if(resultType.length == 0 || resultTitle.length == 0 ){
			$('#alertmsg').text("研究類型與研究名稱不能為空");
			$('#alertModal').modal('show');
		}else{
			$("#Modal_resultType").val(resultType);
			$('.selectpicker').selectpicker('refresh');
			
			$('#Modal_resultTitle').val(resultTitle);
			$('#formModal').modal('show');
		}
		
		return false;
	});
	
	$('.close-btn').click(function(){
		modalClose(false);
	});
	
	$('.sendAdd-btn').click(function(){
		console.log('title:'+  $('#Modal_resultTitle').val());
		console.log('type:'+  $('#Modal_resultType').val());
		console.log('anothers:'+  $('#Modal_resultAnothers').val());
		console.log('unit:'+  $('#Modal_resultUnit').val());
		console.log('page:'+  $('#Modal_resultPage').val());
		console.log('vol:'+  $('#Modal_resultVolumes').val());
		console.log('other:'+  $('#Modal_resultOtherInfo').val());
		
		//$('.sendAdd-btn').attr('disabled', true);
		$("button").attr('disabled', true);
		
		$.ajax({
			url: '/api/product.servlet',
			data: {
				'action': 'create',
				'title' : $('#Modal_resultTitle').val(),
				'type' : $('#Modal_resultType').val(),
				'anothers' : $('#Modal_resultAnothers').val(),
				'unit' : $('#Modal_resultUnit').val(),
				'page' : $('#Modal_resultPage').val(),
				'vol': $('#Modal_resultVolumes').val(),
				'other' : $('#Modal_resultOtherInfo').val(),
				//'isDisable' : $('#Modal_researchDisplay').bootstrapSwitch('state'),
			},
			type:"POST",
			dataType:'json',
			success: function(msg){
				
				//var status = JSON.stringify(msg);//JSON.parse(msg);
				console.log(msg.status);
				
				var StatusInfoText = "";
				
				if(msg.status== true)
					StatusInfoText = "儲存成功......該狀態列將於5秒後自動關閉";
				else
					StatusInfoTextg = "儲存失敗......該狀態列將於5秒後自動關閉";
					
				ShowStatusBar(StatusInfoText, msg.status);
				
			},error:function(xhr, ajaxOptions, thrownError){ 
				$(this).attr('disabled', false);
				console.log(xhr.status);
				console.log(thrownError);
				
				StatusInfoTextg = "儲存失敗......該狀態列將於5秒後自動關閉";
				ShowStatusBar(StatusInfoTextg, false);
			}
		});
		
		return false;
	});
	
	function ShowStatusBar(msg, isStatus){
		console.log(msg);
		$(".statusBar").text(msg);
		
		if(isStatus){
			$(".statusBar").removeClass('bg-warning').addClass('bg-danger');
		}else{
			$(".statusBar").removeClass('bg-danger').addClass('bg-warning');
		
		}$(".statusBar").show();
		
		setTimeout(updateDone, 5000);
	}
	
	function sleep(sec) {
	    var time = new Date().getTime();
	    while(new Date().getTime() - time < sec * 1000);
	}
	
	function updateDone(){
		$(".statusBar").hide();
		modalClose(true);
	}
	
	function modalClose(isReload){
		$('.modal').modal('hide');
		$('.sendAdd-btn').attr('disabled', false);
		
		if(isReload)
			location.reload();
	}
});