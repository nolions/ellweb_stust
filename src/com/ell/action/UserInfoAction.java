package com.ell.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ell.models.products;
import com.ell.models.another;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class UserInfoAction extends ActionSupport{
	private String Id;
	private another userinfo;
	private List<products> ProductList = new ArrayList<>();
	
	public String getId(){
		return Id;
	}
	
	public another getUserinfo(){
		return userinfo;
	}
	
	public List<products> getProductlist(){
		return ProductList;
	}
	
	public void setId(String Id){
		this.Id = Id;
	}
	
	public void setUserinfo(String UserId){
		another model = new another(UserId);
		userinfo = model.getData();
	}
	
	public void setProductlist(String productType){
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("type", productType);
		
		products model = new products();
		ProductList = model.getDataList(paramsMap, -1);
	}
	
	public String execute() {
		
		this.setUserinfo(this.Id);
		return "SUCCESS";
	}
}
