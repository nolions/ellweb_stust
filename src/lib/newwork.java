package lib;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;

public class newwork {
	static ActionContext context;
	public static HttpServletRequest request ;
	public static HttpServletResponse response;
	
	public newwork(){
		setActionContext();
		//setHttpServletRequest();
		//setHttpServletResponse();
	}
	
	static public HashMap<String, Object> parserRequestParam(HttpServletRequest req) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		Enumeration<String> parameterNames = req.getParameterNames();
		
		while (parameterNames.hasMoreElements()) {
			String paramKey = parameterNames.nextElement();
			String paramValue = req.getParameter(paramKey);
			System.out.println("lib.newwork::parserRequestParam() " + paramKey + " : " + paramValue);
			map.put(paramKey, paramValue);
			
			/*		String[] paramValues = request.getParameterValues(paramName);
			for (int i = 0; i < paramValues.length; i++) {
				String paramValue = paramValues[i];
				System.out.println("value :" + paramValue);
				
			}*/
		}
		
		return map;
	}
	
	static public String getIPAddress(){
		setHttpRequest();
		String ip = request.getHeader("x-forwarded-for");
		
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		
		return ip;
	}
	
	private ActionContext setActionContext(){
		if(context == null)
			context = ActionContext.getContext();
		
		return context;
	}
	
//	public static HttpServletRequest setHttpServletRequest(){
//		if(request == null)
//			request = (HttpServletRequest) context.get(ServletActionContext.HTTP_REQUEST);
//		
//		return request;
//	}
//	
//	public static  HttpServletResponse setHttpServletResponse(){
//		if(response == null)
//			response = ServletActionContext.getResponse();
//		
//		return response;
//	}
	
	public static HttpServletRequest setHttpRequest(){
		if(context == null)
			context = ActionContext.getContext();
		
		if(response == null)
			request = (HttpServletRequest) context.get(ServletActionContext.HTTP_REQUEST);
		
		return request;
	}
	
	public static  HttpServletResponse setHttpResponse(){
		if(response == null)
			response = ServletActionContext.getResponse();
		
		return response;
	}
	
//	public static void sendPost(){
//		if(response == null)
//			response = ServletActionContext.getResponse();
//		
//		try {
//			response.sendRedirect("/close.jsp");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
}
