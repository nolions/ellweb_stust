package com.ell.action.backend;

import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class ConsoleAction extends ActionSupport {
	
	private String url;

	public String getUrl()
	{
	 return url;
	}
	
	public ConsoleAction(){
		
	}
	
	public String execute(){
    	
    	return SUCCESS;
    }
    
    public String index() {
            return "index";
    }
    
    public String devlog() {
        return "devlog";
}
    
    public String error() {
        return SUCCESS;
    }
}
